import React, {Component} from 'react';
import About from "../component/ABOUT/About";
import Home from "../component/home/Home";
import Catalog from "../component/catalog/catalog";
import OrdersHistory from "../component/order_history/order_history";
import MngUsers from "../component/mngUsers/mngUsers";
import MngItems from "../component/mngItems/mngItems";
import Blog_Chat from "../component/blog_chat/blog_chat";
import Profil from "../component/profil/profil";
import Join from "../component/blog_chat/join/join";
import Chat from "../component/blog_chat/chat/chat"
import Contact from "../component/contact/Contact.js";

import {Switch, Route} from 'react-router-dom';

class Body extends Component {
    render() {
        return (

            <Switch>
                <Route exact path='/' component={About}/>
                <Route path='/home' component={Home}/>
                <Route exact path='/about' component={About}/>
                <Route exact path='/contact' component={Contact}/>
                <Route exact path='/catalog' component={Catalog}/>
                <Route exact path='/orders_history' component={OrdersHistory}/>
                <Route exact path='/management_users' component={MngUsers}/>
                <Route exact path='/management_items' component={MngItems}/>
                <Route exact path='/blog_chat' component={Blog_Chat}/>
                <Route exact path='/blog_chat/join' component={Join}/>
                <Route  path='/chat' component={Chat}/>
                <Route path="/profil/:name" component={Profil} />
            </Switch>
        );
    }
}


export default Body;


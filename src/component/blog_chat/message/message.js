import React, { Component } from 'react';
import ReactEmoji from 'react-emoji';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import './message.css';

class Message extends Component{

    constructor(props) {
        super(props);
        const {message, name} = this.props;
        const {user, text, type} = message;
        this.state.text = text;
        this.state.user = user;
        this.state.name = name;
        this.state.type = type;
    }

    state = {
        text: null,
        user: null,
        name: null,
        type: null,
        isSentByCurrentUser: false,
        trimmedName: '',
        likeClicked: false,
        unLikeClicked: false
    }

    componentDidMount() {
        const trimmedName = this.state.name.trim().toLowerCase();
        if(this.state.user === trimmedName) {
            this.setState({isSentByCurrentUser: true, trimmedName: trimmedName});
        }
    }

    render() {
        return (
            this.state.isSentByCurrentUser
                ? (
                    this.state.type === 'text' ?
                        <div className="messageContainer justifyEnd">
                            <p className="sentText pr-10">{this.state.trimmedName}</p>
                            <div className="messageBox backgroundBlue">
                                <p className="messageText colorWhite">{ReactEmoji.emojify(this.state.text)}</p>
                            </div>
                        </div>
                        :
                        <div className="messageContainer justifyEnd">
                            <p className="sentText pr-10">{this.state.trimmedName}</p>
                            {this.state.text.map((image) =>
                                <div className="messageBox backgroundBlue">
                                    <img className="imageMessage" src={image.data} alt=""/></div>)}
                        </div>
                )
                : (
                    this.state.type === 'text' ?
                       <div> <div className="messageContainer justifyStart">
                            <div className="messageBox backgroundLight">
                                <p className="messageText colorDark">{ReactEmoji.emojify(this.state.text)}</p>
                            </div>
                            <p className="sentText pl-10 ">{this.state.user}</p>
                        </div>
                           {this.state.unLikeClicked?(<ThumbDownIcon className="likeClicked" />):(  <ThumbDownIcon className="like" onClick={()=>{this.setState({unLikeClicked: true, likeClicked: false})}}/>)}
                           {this.state.likeClicked?(<ThumbUpIcon className="unlikeClicked"/>):(<ThumbUpIcon className="unlike" onClick={()=>{this.setState({likeClicked: true, unLikeClicked: false})}}/>)}
                       </div>: <div><div className="messageContainer justifyStart">
                            {this.state.text.map((image) => <div className="messageBox backgroundLight"><img
                                    className="imageMessage" src={image.data} alt=""/>
                                </div>
                            )}
                            <p className="sentText pl-10 ">{this.state.user}</p>
                        </div>
                            {this.state.unLikeClicked?(  <ThumbDownIcon className="likeClicked" />):(  <ThumbDownIcon className="like" onClick={()=>{this.setState({unLikeClicked: true, likeClicked: false})}}/>)}
                            {this.state.likeClicked?(<ThumbUpIcon className="unlikeClicked"/>):(<ThumbUpIcon className="unlike" onClick={()=>{this.setState({likeClicked: true, unLikeClicked: false})}}/>)}
                        </div>
                )
        );
    }
}

export default Message;
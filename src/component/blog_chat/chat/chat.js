import React, { Component} from 'react';
import axios from 'axios';
import io from "socket.io-client";
import ScrollToBottom from 'react-scroll-to-bottom';
import onlineIcon from '../../../icons/onlineIcon.png';
import closeIcon from '../../../icons/closeIcon.png';
import Message from '../message/message';
import { DropzoneDialogBase } from 'material-ui-dropzone';
import Button from '@material-ui/core/Button';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import Fade from '@material-ui/core/Fade';
import CircularProgress from '@material-ui/core/CircularProgress';
import Avatar from '@material-ui/core/Avatar';
import { AvatarGroup } from '@material-ui/lab';
import './chat.css';

let socket;

class Chat extends Component{

    constructor(props) {
        super(props);
        this.state.name=this.props.location.name;
        this.state.room = this.props.location.room;
        this.state.ENDPOINT = "http://127.0.0.1:5000";
        this.state.userId = this.props.location.userId;
    }

    state = {
        name: null,
        room: null,
        intervalIsSet: false,
        socket: null,
        ENDPOINT: '',
        users: [],
        massage: '',
        massages:[],
        join: false,
        userId: '',
        customers: [],
        sendMessagesCustomer: [],
        historyMessages: [],
        roomsCustomer: [],
        open: false,
        fileObjects: [],
        indexRoom: null,
        loading: true,
        showWelMs:true
    }

    componentDidMount() {
        socket = io(this.state.ENDPOINT);

        this.getMessageOfRoom(this.state.name,this.state.room);

        socket.on('message', massage=>{
            let messages = this.state.massages;
            if((messages.length===0 && massage.user==='admin' && this.state.roomsCustomer[this.state.indexRoom].messages.length===0 )
                ||(massage.user!=='admin'||(massage.user=='admin' &&!massage.text.includes('welcome to room'))))
            {//show welcome massage only at the first time in room
                messages[messages.length] = massage;
                this.setState({massages: messages});
                let sendsForSet = [];
                this.state.sendMessagesCustomer.forEach((message)=>{
                    sendsForSet[sendsForSet.length] = message;
                });
                sendsForSet[sendsForSet.length] = massage
                this.setState({sendMessagesCustomer: sendsForSet});
                this.updateCustomerRooms(sendsForSet);
            }
        });

        socket.on("roomData", ({ users }) => {
            this.setState({users: users});
        });
    }

    getMessageOfRoom=(name,room)=>{
        fetch('http://localhost:3001/api/getCustomerData')
            .then((data) => data.json())
            .then((res) => {
                res.data.forEach((customer)=>{
                    if(customer._id===this.state.userId){
                        this.setState({roomsCustomer: customer.rooms});
                        let roomExists = false;
                        let i = 0;
                        for(;i<customer.rooms.length;i++){
                            if(customer.rooms[i].room===this.state.room){
                                roomExists = true;
                                let history = []
                                let sends = [];
                                customer.rooms[i].messages.forEach((message)=>{
                                    history[history.length] = message;
                                    sends[sends.length] = message;
                                });
                                this.setState({sendMessagesCustomer:sends, historyMessages: history, indexRoom: i, loading: false, showWelMs: false});
                                break;
                            }
                        }

                        if(!roomExists){
                            let rooms = this.state.roomsCustomer;
                            let len = rooms.length;
                            rooms[rooms.length] = {room: this.state.room, messages:[]};
                            this.setState({roomsCustomer: rooms, sendMessagesCustomer: [], indexRoom: len, loading: false});
                            axios.post('http://localhost:3001/api/updateCustomer', {
                                id: this.state.userId,
                                update: { rooms:  rooms},
                            })
                                .then(res => {
                                })
                                .catch((err) => {
                                    console.log(err);
                                });
                        }
                        let image = customer.image;
                        let userId = this.state.userId;
                        socket.emit('join', { name, room, image, userId  }, (error) => {
                            if(error) {
                                alert(error);
                            }
                        })
                    }
                });
            });

    }

    sendMessage= (event, type)=>{
        event.preventDefault();
        if(this.state.massage){
           socket.emit('sendMessage', this.state.massage, type, () => this.setState({massage: ''}));
        }
    }

    clickOnSave=()=>{
        this.setState({open: false});
        if(this.state.fileObjects.length>0){
            socket.emit('sendMessage', this.state.fileObjects, 'image',  () => this.setState({fileObjects: []}));
        }
    }

    updateCustomerRooms = (messages) => {
        let rooms = this.state.roomsCustomer;
        let msgs = []
        messages.forEach((message) => {
            msgs[msgs.length] = message;
        })
        rooms[this.state.indexRoom].messages = msgs;
        this.setState({roomsCustomer: rooms});
        axios.post('http://localhost:3001/api/updateCustomer', {
            id: this.state.userId,
            update: {rooms: rooms},
        }).catch((err) => {
                console.log(err);
            });
    }

    render() {
        return (
            <div className="outerContainer">
                <div className="innerContainer">
                    <div className="infoBar">
                        <div className="leftInnerContainer">
                            <img className="onlineIcon" src={onlineIcon} alt="online icon"/>
                            <h3>{this.state.room}</h3>
                        </div>
                        <div className="rightInnerContainer">
                            <img src={closeIcon} alt="close icon"/>
                        </div>
                    </div>
                    <ScrollToBottom className="messages">
                        {this.state.loading &&
                        <Fade
                            in={true}
                            style={{marginLeft: '45%', marginTop: '150px', height: '150px', width: '150px'}}
                            unmountOnExit>
                            <CircularProgress/>
                        </Fade>}
                        {this.state.historyMessages.map((message, i) =>
                            <div key={i}>
                                <Message message={message} name={this.state.name}/>
                            </div>)}
                        {this.state.massages.map((message, i) =>
                            <div key={i}>
                                <Message message={message} name={this.state.name}/>
                            </div>)}
                    </ScrollToBottom>
                    <div className="back-image">
                        <AvatarGroup >
                        {this.state.users.map((user)=>
                            user.userId!==this.state.userId &&(
                            user.image?( <Avatar style={{margin: "10px"}} alt="" src={user.image} />):(
                                <Avatar style={{margin: "10px"}}  >{user.name[0].toUpperCase()}</Avatar>
                            ) ))}
                        </AvatarGroup>
                            </div>
                    <form className="formForM">
                        <Button variant="contained" color="primary" onClick={() => this.setState({open: true})}>
                            <AddPhotoAlternateIcon fontSize={"large"}/>
                        </Button>
                        <input
                            className="inputForM"
                            type="text"
                            placeholder="Type a message..."
                            value={this.state.massage}
                            onChange={(e) => this.setState({massage: e.target.value})}
                            onKeyPress={event => event.key === 'Enter' ? this.sendMessage(event, 'text') : null}
                        />
                        <button className="sendButton" onClick={(e) => {
                            this.sendMessage(e, 'text')
                        }}>Send
                        </button>
                    </form>
                </div>
                <DropzoneDialogBase
                    acceptedFiles={['image/*']}
                    dropzoneText={"Drag and drop an image here or click"}
                    dialogTitle={"upload Image"}
                    fileObjects={this.state.fileObjects}
                    filesLimit={1}
                    cancelButtonText={"cancel"}
                    submitButtonText={"send"}
                    maxFileSize={5000000}
                    open={this.state.open}
                    onAdd={newFileObjs => {
                        this.setState({fileObjects: [].concat(this.state.fileObjects, newFileObjs)});
                    }}
                    onDelete={deleteFileObj => {
                        let index = 0;
                        for (let i = 0; i < this.state.fileObjects.length; i++) {
                            if (this.state.fileObjects[i].data === deleteFileObj.data) {
                                index = i;
                                break;
                            }
                        }
                        this.state.fileObjects.splice(index, 1);
                    }}
                    onClose={() => this.setState({open: false, fileObjects: []})}
                    onSave={() => {this.clickOnSave()}}
                    showPreviews={true}
                    showFileNamesInPreview={true}
                    getFileAddedMessage={fileName => `Image ${fileName} successfully added.`}
                    getFileRemovedMessage={fileName => `Image ${fileName} removed.`}
                />
            </div>
        );
    }
}

export default Chat;
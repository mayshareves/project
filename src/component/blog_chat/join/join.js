import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './join.css';

class Join extends Component{

    constructor(props) {
        super(props);
        this.state.name = this.props.location.userName;
        this.state.userId = this.props.location.userId;
    }

    state = {
        name: null,
        room: null,
        intervalIsSet: false,
        userName: '',
        userId: ''
    }
    componentDidMount() {
        // if (!this.state.intervalIsSet) {
            // let interval = setInterval(this.getCartsFromDB, 2000);
            // this.setState({ intervalIsSet: interval });
        // }
    }

    // never let a process live forever
    // always kill a process everytime we are done using it
    componentWillUnmount() {
        if (this.state.intervalIsSet) {
            clearInterval(this.state.intervalIsSet);
            this.setState({ intervalIsSet: null });
        }
    }
    render(){
        return(
            <div className="joinOuterContainer">
                <div className="joinInnerContainer">
                    <h1 className="heading">Join</h1>
                    <div>
                        <input placeholder="Name" value={this.state.name} className="joinInput" type="text"  />
                    </div>
                    <div>
                        <input placeholder="Room" className="joinInput mt-20" type="text" onChange={(event) => this.setState({room: event.target.value})} />
                    </div>
                    <Link to={{pathname: '/chat', userId: this.state.userId, name: this.state.name, room: this.state.room }} onClick={(e) =>  ( !this.state.name || !this.state.room) ? e.preventDefault() : null } >
                        <button className="button mt-20" type="submit">Sign In</button>
                    </Link>
                </div>
            </div>
        );
    }

}

export default Join;
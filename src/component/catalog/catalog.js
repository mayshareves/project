import React, {Component} from 'react';
import '../../containers/App.css';
import './catalog.css';
import axios from 'axios';
import Icon from "@material-ui/core/Icon";
import TextField from '@material-ui/core/TextField';

import CardMedia from '@material-ui/core/CardMedia';

class Catalog extends Component{

    constructor(props) {
        super(props);
        this.state.user_id = this.props.location.userId.userId;
        this.state.userName = this.props.location.userName.userName;
    }

    state = {
        data: [],
        carts: [],
        packages: [],
        photographers: [],
        albums: [],
        photos: [],
        intervalIsSet: false,
        idToDelete: null,
        idToUpdate: null,
        objectToUpdate: null,
        userId: null,
        idCart: null,
        items: [],
        packageToShow: null,
        photographerToShow: null,
        albumToShow: null,
        photoToShow: null,
        typeToShow: null,
        amount: null,
        userName: null,
        user_id: null
    };

    componentDidMount() {
        this.getCustomerDataFromDB();
        this.getCartsFromDB();
        this.getPackagesFromDB();
        this.getPhotographerromDB();
        this.getAlbumDataFromDB();
        this.getPhotoDataFromDB();
        this.setState({amount: 1});
    }

    // never let a process live forever
    // always kill a process everytime we are done using it
    componentWillUnmount() {
        if (this.state.intervalIsSet) {
            clearInterval(this.state.intervalIsSet);
            this.setState({ intervalIsSet: null });
        }
    }

    getCustomerDataFromDB=()=>{
        fetch('http://localhost:3001/api/getCustomerData')
            .then((data) => data.json())
            .then((res) => this.setState({ data: res.data }));
    }

    getAlbumDataFromDB=()=>{
        fetch('http://localhost:3001/api/getAlbumData')
            .then((data) => data.json())
            .then((res) => this.setState({ albums: res.data }));
    }

    getPhotoDataFromDB=()=>{
        fetch('http://localhost:3001/api/getPhotoData')
            .then((data) => data.json())
            .then((res) => this.setState({ photos: res.data }));
    }

    getPackagesFromDB=()=>{
        fetch('http://localhost:3001/api/getPackages')
            .then((data) => data.json())
            .then((res) => this.setState({ packages: res.data }));
    }

    getCartsFromDB=()=>{
        fetch('http://localhost:3001/api/getShoppingCartsData')
            .then((data) => data.json())
            .then((res) =>{
                res.data.forEach((cart)=>{
                    if (cart.idCustomer === this.state.user_id) {
                        this.setState({items: cart.items, idCart: cart._id});
                    }
                });
              });
    }

    getPhotographerromDB=()=>{
        fetch('http://localhost:3001/api/getPhotograghersData')
            .then((data) => data.json())
            .then((res) => this.setState({ photographers: res.data }));
    }

    getItems=()=>{
        fetch('http://localhost:3001/api/getShoppingCartsData')
            .then((data) => data.json())
            .then((res) =>{
                res.data.forEach((cart)=>{
                    if (cart._id ===this.state.idCart) {
                        this.setState({items: cart.items});
                    }
                });
            });
    }

    refreshData=()=>{
        this.getCustomerDataFromDB();
        this.getCartsFromDB();
    }

    addItem = (id,category,name, amount, typeButton, price) => {
        let items = this.state.items;
        let newItemExists = false;
        for(let i=0;i<items.length;i++){
            if(items[i].id===id){
                newItemExists = true;
                if(typeButton==='modalButton'){
                    items[i].amount = amount;
                }
                break;
            }
        }
        if (!newItemExists) {
            items[items.length] = {id: id, data: name, amount: amount, price: price, category: category};// add the item to the cart
        }
        axios.post('http://localhost:3001/api/putNewItemToBassket', {
            id: this.state.idCart,
            update: {items: items}
        }).then(()=>{
            this.getItems();
        });
    }

    reviewItem=(item, type)=>{
        let  amount=1;
        let itemsFromCart=[];
        console.log('this.state.items: ', this.state.items);
        if (this.state.items.length === 0) {
            this.state.carts.forEach((datCart) => {
                if (datCart.idCustomer === this.state.user_id) {// the cart of the user
                    itemsFromCart = datCart.items;
                }
            });
        } else {
            itemsFromCart = this.state.items;
        }
        console.log('itemsFromCart: ',itemsFromCart);
        itemsFromCart.forEach((dat)=>{
            if(dat.id===item._id){
                amount=dat.amount;
            }
        });
        switch (type) {
            case 'package':
                this.setState({packageToShow: item, typeToShow: type, amount: amount});
                break;
            case 'photographer':
                this.setState({photographerToShow: item, typeToShow: type, amount: amount});
                break;
            case 'album':
                this.setState({albumToShow: item, typeToShow: type, amount: amount});
                break;
            case 'photo':
                this.setState({photoToShow: item, typeToShow: type, amount: amount});
                break;
        }
    }

    render() {
        return (
            <div>

        <section class="team-main text-center py-5 py-3">
            <div class="container">
            <div class="inner-sec-w3ls py-lg-5 py-3">
                <h2 class="title-wthree text-center mb-lg-5 mb-3">Affordable packages</h2>
                <div class="row t-in mt-3">
                    {this.state.packages.length <= 0
                        ? 'NO DB ENTRIES YET'
                        : this.state.packages.map((dat) => (
                            <div className="col-md-4 price-main-info text-center" data-aos="fade-up">
                                <div className="price-inner card box-shadow">
                                    <div className="card-body " >
                                        <div style={{cursor: 'pointer'}} data-toggle="modal"  data-target="#itemModal" onClick={()=>this.reviewItem(dat,'package')}>
                                        <h4 className="text-uppercase mb-3">{dat.type}</h4>
                                        <h5 className="card-title-wthree pricing-card-title-wthree">
                                           {dat.cost}
                                        </h5>
                                        <ul className="list-unstyled mt-3 mb-4">
                                            <li>{dat.numPhotogragher} Photographs</li>
                                            <li>{dat.numHours} Hour</li>
                                            <li>{dat.typePhotos}</li>
                                            <li>{dat.place}</li>
                                        </ul>
                                        </div>
                                        <button className="hover-2 btn text-uppercase" onClick={() => this.addItem(dat._id,"package",dat.type, 1, 'regularButton', dat.cost)}><Icon className="fa fa-cart-plus" /></button>
                                    </div>
                                </div>
                            </div>
                        ))}

                </div>

            </div>
        </div>
    </section>
                <section class="team-main text-center py-5 py-3">
                    <div class="container">
                    <div class="inner-sec-w3ls py-lg-5 py-3">
                        <h2 class="title-wthree text-center mb-lg-5 mb-3">Our Photographers</h2>
                        <div class="row mt-lg-5 mt-3">
                            {this.state.photographers.length <= 0
                                ? 'NO DB ENTRIES YET'
                                : this.state.photographers.map((dat) => (
                                    <div className="col-md-4 col-sm-6 team-gd " data-aos="zoom-in">
                                        <div className="box16">
                                            <img src={dat.image} alt=" " className="img-fluid" style={{cursor: 'pointer'}} data-toggle="modal"  data-target="#itemModal"onClick={()=>this.reviewItem(dat,'photographer')}/>

                                            <div className="box-content">
                                                <h3 className="title-wthree">{dat.name}</h3>
                                                <div> <span className="post">cost: {dat.cost} per hour</span></div>
                                                <div> <span className="post">phone: {dat.phoneNumber}</span>
                                                </div>
                                            </div>
                                            <button className="hover-2 btn text-uppercase" onClick={() => this.addItem(dat._id,"photographer",dat.name, 1, 'regularButton', dat.cost)}><Icon className="fa fa-cart-plus" /></button>
                                        </div>
                                    </div>
                                ))}
                        </div>
                    </div>
            </div>
    </section>
                <section className="team-main text-center py-5 py-3">
                    <div className="container">
                        <div className="inner-sec-w3ls py-lg-5 py-3">
                            <h2 className="title-wthree text-center mb-lg-5 mb-3">Album Design</h2>
                            <div className="row mt-lg-5 mt-3">
                                {this.state.albums.length <= 0
                                    ? 'NO DB ENTRIES YET'
                                    : this.state.albums.map((dat) => (
                                        <div className="col-md-4 col-sm-6 team-gd " data-aos="zoom-in">
                                            <div className="box16">
                                                <img src={dat.image} alt=" " className="img-fluid" style={{cursor: 'pointer'}} data-toggle="modal"  data-target="#itemModal"
                                                     onClick={()=>this.reviewItem(dat,'album')}/>
                                                <div className="box-content">
                                                    <h3 className="title-wthree">{dat.name}</h3>
                                                    <div><span className="post">cost: {dat.cost}</span></div>
                                                    <div><span className="post">page size: {dat.sizePage}</span>
                                                    </div>
                                                    <div><span className="post">image number: {dat.photosNumber}</span></div>
                                                    <div><span className="post">pages number: {dat.numPage}</span></div>
                                                </div>
                                                <button className="hover-2 btn text-uppercase" onClick={() => this.addItem(dat._id,"album",dat.name, 1, 'regularButton', dat.cost)}><Icon className="fa fa-cart-plus" /></button>
                                            </div>
                                        </div>
                                    ))}
                            </div>
                        </div>
                    </div>
                </section>
                <section className="team-main text-center py-5 py-3">
                    <div className="container">
                        <div className="inner-sec-w3ls py-lg-5 py-3">
                            <h2 className="title-wthree text-center mb-lg-5 mb-3">Photos</h2>
                            <div className="row mt-lg-5 mt-3">
                                {this.state.photos.length <= 0
                                    ? 'NO DB ENTRIES YET'
                                    : this.state.photos.map((dat) => (
                                        <div className="col-md-4 col-sm-6 team-gd aos-animate" data-aos="zoom-in">
                                            <div className="box16">
                                                <img src={dat.image} alt=" " className="img-fluid" style={{cursor: 'pointer'}} data-toggle="modal"  data-target="#itemModal"
                                                     onClick={()=>this.reviewItem(dat,'photo')}/>
                                                <div className="box-content">
                                                    <h3 className="title-wthree">category: {dat.category}</h3>
                                                    <div><span className="post">cost: {dat.cost}</span></div>
                                                    <div><span className="post">size: {dat.size}</span>
                                                    </div>
                                                </div>
                                                <button className="hover-2 btn text-uppercase" onClick={() => this.addItem(dat._id,"photo",dat.name, 1, 'regularButton', dat.cost)}><Icon className="fa fa-cart-plus" /></button>
                                            </div>
                                        </div>
                                    ))}
                            </div>
                        </div>
                    </div>
                </section>
                <div className="modal fade" id="itemModal" tabIndex="-1" role="dialog"
                     aria-labelledby="cartsModalLabel" aria-hidden="true">
                    <div className="modal-dialog margin-top2" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <form>
                                    {this.state.typeToShow === 'package' ? (
                                        <div>
                                            <div className="col-md-4 price-main-info text-center" data-aos="fade-up">
                                                <div className="price-inner card box-shadow" style={{width: "200px"}}>
                                            <h4 className="text-uppercase mb-3">{this.state.packageToShow.type}</h4>
                                            <h5 className="card-title-wthree pricing-card-title-wthree">
                                                {this.state.packageToShow.cost}
                                            </h5>
                                            <ul className="list-unstyled mt-3 mb-4">
                                                <li>{this.state.packageToShow.numPhotogragher} Photographs</li>
                                                <li>{this.state.packageToShow.numHours} Hour</li>
                                                <li>{this.state.packageToShow.typePhotos}</li>
                                                <li>{this.state.packageToShow.place}</li>
                                            </ul>
                                                </div>
                                        </div>
                                            <h5 style={{padding: "50px 10px 10px 230px"}}>Affordable package</h5>
                                            <TextField
                                                style={{margin: "0px 0px 0px 120px", width: "60px"}}
                                                id="standard-number"
                                                label="amount"
                                                type="number"
                                                inputProps={{ min: 1, max: 10, step: 1 }}
                                                value={this.state.amount}
                                                onChange={(e) => this.setState({amount: e.target.value})}
                                            />
                                            <div data-dismiss="modal" style={{margin:"40px 150px 0px 280px", cursor: 'pointer'}} onClick={() => this.addItem(this.state.packageToShow._id,"package",this.state.packageToShow.type, this.state.amount, 'modalButton',this.state.packageToShow.cost)}>
                                            <Icon className="fa fa-cart-plus"  />
                                            </div>
                                        </div>
                                    ) : (
                                        <div></div>
                                    )}
                                    {this.state.typeToShow === 'photographer'?(
                                        <div>
                                            <div className="col-md-4 price-main-info text-center" data-aos="fade-up">
                                                <div className="price-inner card box-shadow" style={{width: "200px"}}>
                                                    <img src={this.state.photographerToShow.image} alt=" " className="img-fluid" />
                                                    <ul className="list-unstyled mt-3 mb-4">
                                                        <h3 className="title-wthree">{this.state.photographerToShow.name}</h3>
                                                        <li>cost: {this.state.photographerToShow.cost} per hour</li>
                                                        <li>phone: {this.state.photographerToShow.phoneNumber}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h5 style={{padding: "100px 10px 10px 250px"}}>Photographer</h5>
                                            <TextField
                                                style={{margin: "0px 0px 0px 120px", width: "60px"}}
                                                id="standard-number"
                                                label="amount"
                                                type="number"
                                                inputProps={{ min: 1, max: 10, step: 1 }}
                                                value={this.state.amount}
                                                onChange={(e) => this.setState({amount: e.target.value})}
                                            />
                                            <div data-dismiss="modal" style={{margin:"40px 150px 0px 280px", cursor: 'pointer'}} onClick={() => this.addItem(this.state.photographerToShow._id,"photographer",this.state.photographerToShow.name, this.state.amount, 'modalButton', this.state.photographerToShow.cost)}>
                                                <Icon className="fa fa-cart-plus" />
                                            </div>
                                        </div>
                                    ):(<div></div>)}
                                    {this.state.typeToShow === 'album'?(
                                        <div>
                                            <div className="col-md-4 price-main-info text-center" data-aos="fade-up">
                                                <div className="price-inner card box-shadow" style={{width: "200px"}}>
                                                    <img src={this.state.albumToShow.image} alt=" " className="img-fluid" />
                                                    <ul className="list-unstyled mt-3 mb-4">
                                                        <h3 className="title-wthree">{this.state.albumToShow.name}</h3>
                                                        <li>cost: {this.state.albumToShow.cost}</li>
                                                        <li>page size: {this.state.albumToShow.sizePage}</li>
                                                        <li>image number: {this.state.albumToShow.photosNumber}</li>
                                                        <li>pages number: {this.state.albumToShow.numPage}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h5 style={{padding: "100px 10px 10px 250px"}}>Album Design</h5>
                                            <TextField
                                                style={{margin: "0px 0px 0px 120px", width: "60px"}}
                                                id="standard-number"
                                                label="amount"
                                                type="number"
                                                inputProps={{ min: 1, max: 10, step: 1 }}
                                                value={this.state.amount}
                                                onChange={(e) => this.setState({amount: e.target.value})}
                                            />
                                            <div data-dismiss="modal" style={{margin:"40px 150px 0px 280px", cursor: 'pointer'}} onClick={() => this.addItem(this.state.albumToShow._id,"album",this.state.albumToShow.name, this.state.amount, 'modalButton', this.state.albumToShow.cost)}>
                                                <Icon className="fa fa-cart-plus" />
                                            </div>
                                        </div>
                                    ):(<div></div>)}
                                    {this.state.typeToShow === 'photo'?(
                                        <div>
                                            <div className="col-md-4 price-main-info text-center" data-aos="fade-up">
                                                <div className="price-inner card box-shadow" style={{width: "200px"}}>
                                                    <img src={this.state.photoToShow.image} alt=" " className="img-fluid" />
                                                    <ul className="list-unstyled mt-3 mb-4">
                                                        <h3 className="title-wthree">category: {this.state.photoToShow.category}</h3>
                                                        <li>cost: {this.state.photoToShow.cost}</li>
                                                        <li>size: {this.state.photoToShow.size}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h5 style={{padding: "100px 10px 10px 250px"}}>Album Design</h5>
                                            <TextField
                                                style={{margin: "0px 0px 0px 120px", width: "60px"}}
                                                id="standard-number"
                                                label="amount"
                                                type="number"
                                                inputProps={{ min: 1, max: 10, step: 1 }}
                                                value={this.state.amount}
                                                onChange={(e) => this.setState({amount: e.target.value})}
                                            />
                                            <div data-dismiss="modal" style={{margin:"40px 150px 0px 280px", cursor: 'pointer'}} onClick={() => this.addItem(this.state.photoToShow._id,"photo",this.state.photoToShow.name, this.state.amount, 'modalButton', this.state.photoToShow.cost)}>
                                                <Icon className="fa fa-cart-plus" />
                                            </div>
                                        </div>
                                    ):(
                                        <div/>
                                    )}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        );
    }
}

export default Catalog;




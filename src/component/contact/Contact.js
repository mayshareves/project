import React, {Component} from 'react';
import emailjs from 'emailjs-com';
import {GoogleLogin, GoogleLogout} from "react-google-login";
import '../../containers/App.css';
import '../../index.css';

class Contact extends Component{

    state = {
        messagebody:null,
        useremail: '',

    };

    sendEmail = (e) => {
        console.log('bbbbbbbb')

        const templateParams={
            email: this.state.useremail,
            messagebody: this.state.messagebody
        };

        emailjs.send('gmail', 'template_tZxscsXu', templateParams, 'user_wtzkikbAWZwwA5CoXG2Ex')
            .then((result) => {
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
    };


    render() {
        return (
            <section className="banner-bottom-w3layouts py-lg-5 py-md-5 py-3">
                <div
                    className="inner-sec-w3ls pt-md-5 pt-md-3 pt-3">
                    < h4
                        className="sub-tittle text-uppercase text-center"> Find
                        us </h4>
                    <h2 className="title-wthree text-center mb-lg-5 mb-3">Contact</h2>
                    <div
                        className="container">
                        < div
                            className="address row mb-5">
                            < div
                                className="col-lg-4 address-grid-w3ls">
                                < div
                                    className="row address-info"
                                    data-aos="fade-up">
                                    < div
                                        className="col-md-3 address-left text-center">
                                        < i
                                            className="fa fa-map"> </i>
                                    </div>
                                    <div className="col-md-9 address-right text-left">
                                        <h6 className="ad-info text-uppercase mb-2">Address</h6>
                                        <p> Herzelia, Israel

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 address-grid-w3ls">
                                <div className="row address-info" data-aos="fade-up">
                                    <div className="col-md-3 address-left text-center">
                                        <i className="fa fa-envelope"></i>
                                    </div>
                                    <div className="col-md-9 address-right text-left">
                                        <h6 className="ad-info text-uppercase mb-2">Email</h6>
                                        <p>
                                            <div data-toggle="modal" id = "contactbtn" data-target="#contactModal">To send email press here</div>
                                            <div className="modal fade" id="contactModal" tabIndex="-1" role="dialog"
                                                 aria-labelledby="loginModalLabel" aria-hidden="true">
                                                <div className="modal-dialog modal-dialog-centered" role="document">
                                                    <div className="modal-content">
                                                        <div className="modal-header">
                                                            <h5 className="modal-title" id="contactModalLabel">Contact us:</h5>
                                                            <button type="button" className="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div className="modal-body">
                                                            <form id="modalForm">
                                                                <div className="form-group">
                                                                    <label htmlFor="userEmail" className="col-form-label">enter your email address</label>
                                                                    <input type="text" className="form-control" id="userEmail"
                                                                           name='userEmail' placeholder='Enter your email'
                                                                           required onChange={(e) => this.setState({useremail: e.target.value})}/>
                                                                </div>
                                                                <div className="form-group">
                                                                    <label htmlFor="messagebody"
                                                                           className="col-form-label">Message body:</label>
                                                                    <input onChange={(e) => this.setState({messagebody: e.target.value})}
                                                                           type="text" className="form-control"
                                                                           id="messagebody" name='messagebody'
                                                                           placeholder='Write your message here' required/>
                                                                </div>
                                                                <div className="modal-footer">
                                                                    <button type="button" className="btn btn-secondary"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                    <input className="btn btn-primary" data-dismiss="modal"
                                                                           id="sendBtnn" type="submit"
                                                                           value="send" onClick={() => this.sendEmail(this)}/>
                                                                </div>
                                                                <div data-dismiss="modal">

                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {/*<a href="mailto:example@email.com"> To send email press here</a>*/}
                                        </p>
                                    </div>

                                </div>
                            </div>
                            <div className="col-lg-4 address-grid-w3ls">
                                <div className="row address-info" data-aos="fade-up">
                                    <div className="col-md-3 address-left text-center">
                                        <i className="fa fa-mobile"></i>
                                    </div>
                                    <div className="col-md-9 address-right text-left">
                                        <h6 className="ad-info text-uppercase mb-2">Phone</h6>
                                        <p>+1 234 567 8901</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-6 map">
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        );
    }
}

export default Contact;
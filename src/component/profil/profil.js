import React, {Component} from 'react'
import axios from 'axios';
import { Input } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountCircle from '@material-ui/icons/AccountCircle';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Grid from "@material-ui/core/Grid";
import Icon from "@material-ui/core/Icon";
import addImageProfile from "./addImageProfile.png";
let crypto = require('crypto');

class Profil extends  Component{

    constructor(props) {
        super(props);
        this.state.userName=this.props.match.params.name;
        this.state.permission = props.location.permission;
        this.state.id = props.location.userId;
    }

    state={
        fullName: null,
        userName: null,
        password: null,
        mail: '',
        phone: '',
        address: '',
        fullNameToUpdate: null,
        userNameToUpdate: null,
        passwordToUpdate: null,
        mailToUpdate: '',
        phoneToUpdate: '',
        addressToUpdate: '',
        permission:null,
        id: null,
        pswSecret: null,
        showPassword: false,
        image: ''
    };

    componentDidMount() {
        this.getUserData();
        this.getPswKey();
    }

    componentWillUnmount() {
        if (this.state.intervalIsSet) {
            clearInterval(this.state.intervalIsSet);
            this.setState({ intervalIsSet: null });
        }
    }

    getUserData=()=>{
        switch (this.state.permission) {
            case 'customer':
                this.getCustomers();
                break;
            case 'worker':
                this.getWorkers();
                break;
            case 'management':
                this.getManagements();
                break;
            default:
            // code block
        }
    }

    getCustomers=()=>{
        fetch('http://localhost:3001/api/getCustomerData')
            .then((data) => data.json())
            .then((res) => {
                res.data.forEach((customer) => {
                    if (customer._id === this.state.id) {
                        this.setState({fullName: customer.fullName, password: customer.password, mail: customer.mail, phone: customer.phone, address: customer.address, image: customer.image});
                        this.setState({fullNameToUpdate: customer.fullName, mailToUpdate: customer.mail, phoneToUpdate: customer.phone, addressToUpdate: customer.address});
                    }
                });
            });
    }

    getWorkers=()=>{
        fetch('http://localhost:3001/api/getWorkers')
            .then((data) => data.json())
            .then((res) => {
                res.data.forEach((worker) => {
                    if (worker._id === this.state.id) {
                        this.setState({fullName: worker.fullName, password: worker.password, mail: worker.mail, phone: worker.phone, address: worker.address, image: worker.image});
                        this.setState({fullNameToUpdate: worker.fullName, mailToUpdate: worker.mail, phoneToUpdate: worker.phone, addressToUpdate: worker.address});
                    }
                });
            });
    }

    getManagements=()=>{
        fetch('http://localhost:3001/api/getManagementData')
            .then((data) => data.json())
            .then((res) => {
                res.data.forEach((management) => {
                    if (management._id === this.state.id) {
                        this.setState({ fullName: management.fullName, password: management.password, mail: management.mail, phone: management.phone, address: management.address, image: management.image});
                        this.setState({ fullNameToUpdate: management.fullName, mailToUpdate: management.mail, phoneToUpdate: management.phone, addressToUpdate: management.address});
                    }
                });
            });
    }

    getPswKey=()=>{
        fetch('http://localhost:3001/api/getpsw')
            .then((data) => data.json())
            .then((res) => this.setState({ pswSecret: res.data[0]['pswSecret'] }));
    }

    updateUserProfil=()=>{
        if(this.state.permission === 'management'){
            this.updateUser('updateManagement');
        }
        if(this.state.permission === 'worker'){
            this.updateUser('updateWorker');
        }
        if(this.state.permission==='customer'){
            this.updateUser('updateCustomer');
        }
    }

    updateUser=(nameOfApi)=>{
        if(this.state.passwordToUpdate){
            let key = crypto.createCipher('aes-128-cbc', this.state.pswSecret);
            key.update(this.state.passwordToUpdate, 'utf8');
            let encPsw = key.final('hex');
            axios.post('http://localhost:3001/api/'+nameOfApi, {
                id: this.state.id,
                update: { userName: this.state.userNameToUpdate, fullName: this.state.fullNameToUpdate, password: encPsw, mail: this.state.mailToUpdate, phone: this.state.phoneToUpdate, address: this.state.addressToUpdate },
            })
                .then(res => {
                    this.setState({userName: this.state.userNameToUpdate});
                    switch (nameOfApi) {
                        case 'updateManagement':
                            this.getManagements();
                            break;
                        case 'updateWorker':
                            this.getWorkers();
                            break;
                        case 'updateCustomer':
                            this.getCustomers();
                            break;
                        default:
                        // code block
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        }else{
            axios.post('http://localhost:3001/api/'+nameOfApi, {
                id: this.state.id,
                update: { fullName: this.state.fullNameToUpdate, mail: this.state.mailToUpdate, phone: this.state.phoneToUpdate, address: this.state.addressToUpdate },
            })
                .then(res => {
                    switch (nameOfApi) {
                        case 'updateManagement':
                            this.getManagements();
                            break;
                        case 'updateWorker':
                            this.getWorkers();
                            break;
                        case 'updateCustomer':
                            this.getCustomers();
                            break;
                        default:
                        // code block
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        }

    }

    render(){
        return(
            <div style={{maxWidth:"550px", margin: "0px auto"}}>
                <div style={{
                    display:"flex",
                    justifyContent:"center",
                    margin:"18px 0px"
                }}>

                    {this.state.image?<div>
                            <img style={{width:"160px",height:"160px",borderRadius:"80px"}}
                                 src={this.state.image}
                            /></div>:
                        <div>
                            <img style={{width:"160px",height:"160px",borderRadius:"80px"}}
                                 src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRH0AKS7GEh68dRGKfYp6oQy1vKsYUdXgBNwg&usqp=CAU"
                            />
                        </div>
                    }
                    <div style={{margin: "20px 0px 0px 40px"}}>
                        <h4>{this.state.fullName}</h4>
                        <div style={{display: "flex"}}>
                            <h6>user name: {this.state.userName}</h6>
                            <h6 style={{margin: "0px 10px 0px "}}>email: {this.state.mail}</h6>
                        </div>
                        <div style={{display: "flex"}}>
                            <h6>phone: {this.state.phone}</h6>
                            <h6 style={{margin: "0px 10px 0px "}}>address: {this.state.address}</h6>
                        </div>
                        <button className=" text-uppercase" data-toggle="modal" data-target="#updateProfil" >update</button>
                    </div>
                </div>
                <div className="Absolute-Center is-Responsive" >
                    <div className="col-sm-12 col-md-10 col-md-offset-1">
                <div className="modal fade" id="updateProfil" tabIndex="-1" role="dialog"
                     aria-labelledby="deleteItemModalLabel" aria-hidden="true">
                    <div className="modal-dialog margin-top2" role="document" >
                        <div className="modal-content" >
                            <div className="modal-header " >
                                <h5 className="modal-title">update profile</h5>
                                <button type="button" className="close" data-dismiss="modal"
                                        aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form >
                                    <div className="form-group">
                                        <FormControl>
                                            <InputLabel htmlFor="component-simple">Name</InputLabel>
                                            <Input id="component-simple"  value={this.state.fullNameToUpdate} onChange={(e) => this.setState({fullNameToUpdate: e.target.value})} />
                                        </FormControl>
                                    </div>
                                    <Grid container  alignItems="flex-end" style={{margin: "10px 0 0"}}>
                                        <Grid item>
                                            <Icon className="fa fa-envelope-square" />
                                        </Grid>
                                        <Grid item>
                                            <TextField id="input-with-icon-grid" label="Email" value={this.state.mailToUpdate}
                                                       onChange={(e) => this.setState({mailToUpdate: e.target.value})}/>
                                        </Grid>
                                    </Grid>
                                    <Grid container  alignItems="flex-end" style={{margin: "10px 0 0"}}>
                                        <Grid item>
                                            <Icon className="fa fa-mobile" />
                                        </Grid>
                                        <Grid item>
                                            <TextField id="input-with-icon-grid" label="Phone" value={this.state.phoneToUpdate}
                                                       onChange={(e) => this.setState({phoneToUpdate: e.target.value})}/>
                                        </Grid>
                                    </Grid>

                                    <Grid container  alignItems="flex-end" style={{margin: "10px 0 0"}}>
                                        <Grid item>
                                            <Icon className="fa fa-map-marker" />
                                        </Grid>
                                        <Grid item>
                                            <TextField id="input-with-icon-grid" label="Address" value={this.state.addressToUpdate}
                                                       onChange={(e) => this.setState({addressToUpdate: e.target.value})}/>
                                        </Grid>
                                    </Grid>

                                    {this.state.password?
                                    <div className="form-group">
                                        <FormControl >
                                            <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                                            <Input
                                                id="standard-adornment-password"
                                                type={this.state.showPassword ? 'text' : 'password'}
                                                onChange={(e) => this.setState({passwordToUpdate: e.target.value})}
                                                endAdornment={
                                                    <InputAdornment position="end">
                                                        <IconButton
                                                            aria-label=" password visibility"
                                                            onClick={() => this.setState({showPassword: !this.state.showPassword})}>
                                                            {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                                        </IconButton>
                                                    </InputAdornment>
                                                }
                                            />
                                        </FormControl>
                                    </div>:<div/>}
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary"
                                                data-dismiss="modal">cancel
                                        </button>
                                        <input className="btn btn-primary" data-dismiss="modal"
                                               type="submit"
                                               value="update"
                                               onClick={() => this.updateUserProfil()}
                                               />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Profil;
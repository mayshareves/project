import React, {Component} from 'react'
import axios from 'axios';
import './mngUsers.css';
import './animate.css';
import '../../containers/App.css';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import TextField from "@material-ui/core/TextField";
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import EditIcon from '@material-ui/icons/Edit';
import Grid from "@material-ui/core/Grid";
import Icon from "@material-ui/core/Icon";
import InputAdornment from "@material-ui/core/InputAdornment";
import AccountCircle from "@material-ui/icons/AccountCircle";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import {Input} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Tooltip from '@material-ui/core/Tooltip';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { green } from '@material-ui/core/colors';
import { DropzoneDialogBase } from 'material-ui-dropzone';
let crypto = require('crypto');

class ManageUsers extends  Component{
    state={
        customersList: [],
        workersList:[],
        managementsList:[],
        idUser: null,
        itemToDelete: null,
        nameToDelete: null,
        name: null,
        idCustomer: null,
        intervalIsSet: false,
        itemToUpdate: null,
        userTypeToUpdate: null,
        nameToUpdate: null,
        phoneToUpdate: null,
        mailToUpdate: null,
        addressToUpdate: null,
        idToUpdate: null,
        typeUserToAdd: null,
        nameToAdd: '',
        familyNameToAdd: '',
        mailToAdd: '',
        phoneToAdd: '',
        addressToAdd: '',
        userNameToAdd: '',
        passwordToAdd: '',
        pswSecret: null,
        showPassword: false,
        imageToAdd: '',
        fileObjects: [],
        open: false
    };
    componentDidMount() {
        this.getWorkersList();
        this.getManagementsList();
        this.getPswKey();
        this.getCustomesList();
    }

    componentWillUnmount() {
        if (this.state.intervalIsSet) {
            clearInterval(this.state.intervalIsSet);
            this.setState({ intervalIsSet: null });
        }
    }

    getPswKey=()=>{
        fetch('http://localhost:3001/api/getpsw')
            .then((data) => data.json())
            .then((res) => this.setState({ pswSecret: res.data[0]['pswSecret'] }));
    }

     getCustomesList =()=>{
         fetch('http://localhost:3001/api/getCustomerData')
            .then((data) => data.json())
            .then((res) => {
                const customersData=[];
                res.data.map((dat)=>{
                    if(dat.valid===true){
                        customersData[customersData.length]=dat;
                    }
                })
                this.setState({ customersList: customersData })});
    }
    getWorkersList=()=>{
        fetch('http://localhost:3001/api/getWorkers')
            .then((data) => data.json())
            .then((res) => {
                const workersData=[];
                res.data.map((dat)=>{
                    if(dat.valid===true){
                        workersData[workersData.length]=dat;
                    }
                })
                this.setState({ workersList: workersData })});
    }
    getManagementsList=()=>{
        fetch('http://localhost:3001/api/getManagementData')
            .then((data) => data.json())
            .then((res) => {
                const managementsData=[];
                res.data.map((dat)=>{
                    if(dat.valid===true){
                        managementsData[managementsData.length]=dat;
                    }
                })
                this.setState({ managementsList: managementsData })});
    }
    deleteCustomer = () => {
        axios.post('http://localhost:3001/api/updateCustomer', {
            id: this.state.idUser,
            update: { valid: 'false' },
        })
            .then(res => {
                this.getCustomesList();
            })
            .catch((err) => {
                console.log(err);
            });
    }
    deleteWorker=()=>{
        axios.post('http://localhost:3001/api/updateWorker', {
            id: this.state.idUser,
            update: { valid: 'false' },
        })
            .then(res => {
                this.getWorkersList();
            })
            .catch((err) => {
                console.log(err);
            });
    }
    deleteManagement=()=>{
        axios.post('http://localhost:3001/api/updateManagement', {
            id: this.state.idUser,
            update: { valid: 'false' },
        })
            .then(res => {
                this.getManagementsList();
            })
            .catch((err) => {
                console.log(err);
            });
    }

    deleteUser = () => {
        switch (this.state.itemToDelete) {
            case 'customer':
                this.deleteCustomer();
                break;
            case 'worker':
                this.deleteWorker();
                break;
            case 'management':
                this.deleteManagement();
                break;
            default:
        }
    }

    updateCustomer= () => {
        axios.post('http://localhost:3001/api/updateCustomer', {
            id: this.state.idToUpdate,
            update: { fullName: this.state.nameToUpdate, phone: this.state.phoneToUpdate,
                mail: this.state.mailToUpdate, address: this.state.addressToUpdate},
        })
            .then(res => {
                this.getCustomesList();
            })
            .catch((err) => {
                console.log(err);
            });
    }

    updateWorker=()=>{
        axios.post('http://localhost:3001/api/updateWorker', {
            id: this.state.idToUpdate,
            update: {fullName: this.state.nameToUpdate, phone: this.state.phoneToUpdate,
                mail: this.state.mailToUpdate, address: this.state.addressToUpdate},
        })
            .then(res => {
                this.getWorkersList();
            })
            .catch((err) => {
                console.log(err);
            });
    }

    updateManagement=()=>{
        axios.post('http://localhost:3001/api/updateManagement', {
            id: this.state.idToUpdate,
            update: {fullName: this.state.nameToUpdate, phone: this.state.phoneToUpdate,
                mail: this.state.mailToUpdate, address: this.state.addressToUpdate},
        })
            .then(res => {
                this.getManagementsList();
            })
            .catch((err) => {
                console.log(err);
            });
    }


    updateUser = () => {
        console.log('updateUser: ',this.state.userTypeToUpdate );
        switch (this.state.userTypeToUpdate) {
            case 'customer':
                console.log('customer');
                this.updateCustomer();
                break;
            case 'worker':
                this.updateWorker();
                break;
            case 'management':
                this.updateManagement();
                break;
            default:
        }
    }

    setUpdateUser = (userType, user) => {
        console.log('type: ', userType, ' user: ', user);
        this.setState({
            nameToUpdate: user.fullName,
            phoneToUpdate: user.phone,
            mailToUpdate: user.mail,
            addressToUpdate: user.address,
            idToUpdate: user._id,
            userTypeToUpdate: userType
        });
    }

    addCustomer=()=>{
        console.log('addCustomer: userName ',this.state.userNameToAdd, 'psw: ', this.state.passwordToAdd )
        let secret = crypto.createCipher('aes-128-cbc', this.state.pswSecret);
        secret.update(this.state.passwordToAdd, 'utf8');
        let encPsw = secret.final('hex');
        axios.post('http://localhost:3001/api/putNewClient', {
            name: this.state.nameToAdd + " "+ this.state.familyNameToAdd,
            username:this.state.userNameToAdd,
            password:encPsw,
            mail: this.state.mailToAdd,
            phone: this.state.phoneToAdd,
            address: this.state.addressToAdd,
            image: this.state.imageToAdd
        }).then(res => {
                this.getCustomesList();
            this.setState({imageToAdd: null })
            })
            .catch((err) => {
                console.log(err);
            });
    }

    addWorker=()=>{
        let secret = crypto.createCipher('aes-128-cbc', this.state.pswSecret);
        secret.update(this.state.passwordToAdd, 'utf8');
        let encPsw = secret.final('hex');
        axios.post('http://localhost:3001/api/putNewWorker', {
            name: this.state.nameToAdd + " "+ this.state.familyNameToAdd,
            username:this.state.userNameToAdd,
            password:encPsw,
            mail: this.state.mailToAdd,
            phone: this.state.phoneToAdd,
            address: this.state.addressToAdd,
            image: this.state.imageToAdd
        }).then(res => {
            this.getWorkersList();
            this.setState({imageToAdd: null })
        })
            .catch((err) => {
                console.log(err);
            });
    }

    addManagement=()=>{
        console.log('addCustomer: userName ',this.state.userNameToAdd, 'psw: ', this.state.passwordToAdd )
        let secret = crypto.createCipher('aes-128-cbc', this.state.pswSecret);
        secret.update(this.state.passwordToAdd, 'utf8');
        let encPsw = secret.final('hex');
        axios.post('http://localhost:3001/api/putNewManagement', {
            name: this.state.nameToAdd + " "+ this.state.familyNameToAdd,
            username:this.state.userNameToAdd,
            password:encPsw,
            mail: this.state.mailToAdd,
            phone: this.state.phoneToAdd,
            address: this.state.addressToAdd,
            image: this.state.imageToAdd
        }).then(res => {
            this.getManagementsList();
            this.setState({imageToAdd: null })
        }).catch((err) => {
                console.log(err);
          });
    }

    addUser=()=>{
        switch (this.state.typeUserToAdd) {
            case 'customer':
                console.log('customer');
                this.addCustomer();
                break;
            case 'worker':
                this.addWorker();
                break;
            case 'management':
                this.addManagement();
                break;
            default:
        }
    }

deleteCustomer=()=>{
    axios.delete('http://localhost:3001/api/deleteCustomer', {
        data: {
            id: this.state.idUser,
        },
    }).then(res => {
        this.getCustomesList();
    })
        .catch((err) => {
            console.log(err);
        });
}

    deleteWorker=()=>{
        axios.delete('http://localhost:3001/api/deleteWorker', {
            data: {
                id: this.state.idUser,
            },
        }).then(res => {
            this.getWorkersList();
        })
            .catch((err) => {
                console.log(err);
            });
    }

    deleteManagement=()=>{
        axios.delete('http://localhost:3001/api/deleteManager', {
            data: {
                id: this.state.idUser,
            },
        }).then(res => {
            this.getManagementsList();
        })
            .catch((err) => {
                console.log(err);
            });
    }

    deleteUser=()=>{
        switch (this.state.itemToDelete) {
            case 'customer':
                console.log('customer');
                this.deleteCustomer();
                break;
            case 'worker':
                this.deleteWorker();
                break;
            case 'management':
                this.deleteManagement();
                break;
            default:
        }
    }

    onSaveImage=()=>{
        console.log('file: ', this.state.fileObjects);
        this.setState({imageToAdd: this.state.fileObjects[0].data })
    }

    render(){
        return(
            <div>

                <h2 className="addUser">Add Users</h2>
                <div className="addButton">
                <Button
                    variant="contained"
                    color="default"
                    size="large"
                    startIcon={<PersonAddIcon />}
                    style={{margin: '5px'}}
                    data-toggle="modal"
                    data-target="#addUserModal"
                    onClick={()=>this.setState({typeUserToAdd: 'customer'})}
                >
                    Customer
                </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        size="large"
                        startIcon={<PersonAddIcon />}
                        style={{margin: '5px'}}
                        data-toggle="modal"
                        data-target="#addUserModal"
                        onClick={()=>this.setState({typeUserToAdd: 'worker'})}
                    >
                        Worker
                    </Button>
                    <Button
                        variant="contained"
                        size="large"
                        color="primary"
                        startIcon={<PersonAddIcon />}
                        style={{margin: '5px', background: '#0d3d67'}}
                        data-toggle="modal"
                        data-target="#addUserModal"
                        onClick={()=>this.setState({typeUserToAdd: 'management'})}
                    >
                        Manager
                    </Button>
                </div>
                <div className="Absolute-Center is-Responsive">
                    <div className="col-sm-12 col-md-10 col-md-offset-1">
                        <div className="modal fade" id="addUserModal" tabIndex="-1" role="dialog"
                             aria-hidden="true">
                            <div className="modal-dialog margin-top2" role="document">
                                <div className="modal-content">
                                    <div className="modal-header ">
                                        <h5 className="modal-title" id="signUpModalLabel">Add {this.state.typeUserToAdd}</h5>
                                        <button type="button" className="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">


                                        <form >
                                            <div style={{marginLeft: "35%"}}>
                                            {this.state.imageToAdd?(
                                                <i><img className="image-profile image-profile-on-add" src={this.state.imageToAdd}/> </i>
                                            ):( <i><img  style={{height: "120px", cursor: "pointer"}} src="images/addImageProfile.png" onClick={() =>  this.setState({open: true})}/> </i>)}
                                                <DropzoneDialogBase
                                                    acceptedFiles={['image/*']}
                                                    fileObjects={this.state.fileObjects}
                                                    cancelButtonText={"cancel"}
                                                    submitButtonText={"submit"}
                                                    maxFileSize={5000000}
                                                    open={this.state.open}
                                                    onAdd={newFileObjs => {
                                                        console.log('onAdd', newFileObjs);
                                                        this.setState({fileObjects: [].concat(this.state.fileObjects, newFileObjs)});
                                                    }}
                                                    onDelete={deleteFileObj => {
                                                        console.log('onDelete', deleteFileObj);
                                                        let index = 0;
                                                        for(let i=0;i<this.state.fileObjects.length;i++){
                                                            if(this.state.fileObjects[i].data===deleteFileObj.data){
                                                                index = i;
                                                                break;
                                                            }
                                                        }
                                                        this.state.fileObjects.splice(index,1);
                                                    }}
                                                    onClose={() => this.setState({open: false})}
                                                    onSave={() => {
                                                        this.setState({open: false});
                                                        this.onSaveImage();
                                                    }}
                                                    showPreviews={true}
                                                    showFileNamesInPreview={true}
                                                />
                                            </div>
                                            <div style={{margin: "0px 0px 20px 0px"}}>
                                                All the fields are required.
                                            </div>

                                            <TextField id="outlined-basic"
                                                       placeholder='Enter personal name' label="Personal Name" variant="outlined" value={this.state.nameToAdd}
                                                       onChange={(e) => this.setState({nameToAdd: e.target.value})}/>
                                            <TextField id="outlined-basic" style={{margin: "0px 10px"}}
                                                       placeholder='Enter family name' label="Family Name" variant="outlined" value={this.state.familyNameToAdd}
                                                       onChange={(e) => this.setState({familyNameToAdd: e.target.value})}/>
                                            <Grid container  alignItems="flex-end" style={{margin: "10px 0 0"}}>
                                                <Grid item>
                                                    <Icon className="fa fa-envelope-square" />
                                                </Grid>
                                                <Grid item>
                                                    <TextField id="input-with-icon-grid" label="Email" value={this.state.mailToAdd}
                                                               onChange={(e) => this.setState({mailToAdd: e.target.value})}/>
                                                </Grid>
                                            </Grid>
                                            <Grid container  alignItems="flex-end" style={{margin: "10px 0 0"}}>
                                                <Grid item>
                                                    <Icon className="fa fa-mobile" />
                                                </Grid>
                                                <Grid item>
                                                    <TextField id="input-with-icon-grid" label="Phone" value={this.state.phoneToAdd}
                                                               onChange={(e) => this.setState({phoneToAdd: e.target.value})}/>
                                                </Grid>
                                            </Grid>

                                            <Grid container  alignItems="flex-end" style={{margin: "10px 0 0"}}>
                                                <Grid item>
                                                    <Icon className="fa fa-map-marker" />
                                                </Grid>
                                                <Grid item>
                                                    <TextField id="input-with-icon-grid" label="Address" value={this.state.addressToAdd}
                                                               onChange={(e) => this.setState({addressToAdd: e.target.value})}/>
                                                </Grid>
                                            </Grid>

                                            <TextField
                                                style={{margin: "10px 0 0"}}
                                                id="input-with-icon-textfield"
                                                label="User Name"
                                                value={this.state.userNameToAdd}
                                                onChange={(e) => this.setState({userNameToAdd: e.target.value})}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position="start">
                                                            <AccountCircle />
                                                        </InputAdornment>
                                                    ),
                                                }}
                                            />

                                            <FormControl style={{margin: "10px 0"}}>
                                                <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                                                <Input
                                                    id="standard-adornment-password"
                                                    type={this.state.showPassword ? 'text' : 'password'}
                                                    onChange={(e) => this.setState({passwordToAdd: e.target.value})}
                                                    value={this.state.passwordToAdd}
                                                    endAdornment={
                                                        <InputAdornment position="end">
                                                            <IconButton
                                                                aria-label=" password visibility"
                                                                onClick={() => this.setState({showPassword: !this.state.showPassword})}>
                                                                {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                                            </IconButton>
                                                        </InputAdornment>
                                                    }
                                                />

                                            </FormControl>

                                            <div className="modal-footer">
                                                <Icon className="fa fa-ban" style={{ cursor: 'pointer' }} data-dismiss="modal" onClick={() => {console.log('1111')}}/>
                                                <Icon className="fa fa-plus-circle" style={{ color: green[500], cursor: 'pointer' }} data-dismiss="modal" onClick={() => {this.addUser()}}/>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="banner-bottom-w3layouts py-lg-5 py-md-5 py-3">
                    <div className="container ">
                      <div className="flex margin ">
                          <div className="margin-left">
                              <div className="col-md-12 margin-bottom">
                                  <h2 className="title animate-box">Customers List</h2>
                              </div>
                              <div className="row t-in mt-3">
                                  {this.state.customersList.length <= 0
                                      ? 'NO DB ENTRIES YET'
                                      : this.state.customersList.map((dat) => (
                                          <div className="col-md-6 col-sm-6">
                                              <div className="feature-center animate-box min-width" data-animate-effect="fadeIn">
                                                <span className="icon">
                                                    {dat.image?(
                                                        <i><img className="image-profile image-profile image-profile-on-add" src={dat.image}/> </i>
                                                    ):( <i><img src="images/userIcon1.png"/> </i>)}

                                                </span>
                                                <h3>Name: {dat.fullName}</h3>
                                                  {dat.phone?(
                                                      <h3>Phone: {dat.phone}</h3>
                                                  ):<div></div>}
                                                  <h3>Mail: {dat.mail}</h3>
                                                  {dat.address?(
                                                      <h3>Address: {dat.address}</h3>
                                                  ):(<div></div>)}

                                                  <Button
                                                      variant="contained"
                                                      color="secondary"
                                                      startIcon={<DeleteIcon />}
                                                      onClick={() => this.setState({
                                                          idUser: dat._id,
                                                          nameToDelete: dat.fullName, itemToDelete: 'customer'
                                                      })}
                                                      data-toggle="modal"
                                                      data-target="#deleteUserModal"
                                                  >
                                                      Delete
                                                  </Button>
                                                <button style={{margin: "5px"}} data-toggle="modal"
                                                        data-target="#UpdateUserModal" onClick={() => this.setUpdateUser('customer',dat)} className="hover-2 btn text-uppercase" >update</button>
                                              </div>
                                          </div>
                                      ))
                                  }
                              </div>
                          </div>
                      </div>
                    </div>
                </section>
                <section className="banner-bottom-w3layouts py-lg-5 py-md-5 py-3">
                    <div className="container ">
                        <div className="flex margin ">
                            <div className="margin-left">
                                <div className="col-md-12 margin-bottom">
                                    <h2 className="title animate-box">Workers List</h2>
                                </div>
                                {this.state.workersList.length <= 0
                                    ? 'NO DB ENTRIES YET'
                                    : this.state.workersList.map((dat) => (
                                        <div className="col-md-6 col-sm-6 " >
                                            <div className="feature-center animate-box min-width" data-animate-effect="fadeIn">
                                                <span className="icon">
                                                    {dat.image?(
                                                        <i><img className="image-profile image-profile image-profile-on-add" src={dat.image}/> </i>
                                                    ):( <i><img src="images/userIcon1.png"/> </i>)}
                                                </span>
                                                <h3>Name: {dat.fullName}</h3>
                                                <h3>Phone: {dat.phone}</h3>
                                                <h3>Mail: {dat.mail}</h3>
                                                <h3>Address: {dat.address}</h3>
                                                <Button
                                                    variant="contained"
                                                    color="secondary"
                                                    startIcon={<DeleteIcon />}
                                                    onClick={() => this.setState({
                                                        idUser: dat._id,
                                                        nameToDelete: dat.fullName,
                                                        itemToDelete: 'worker'
                                                    })}
                                                    data-toggle="modal"
                                                    data-target="#deleteUserModal"
                                                >
                                                    Delete
                                                </Button>
                                                <button style={{margin: "5px"}} data-toggle="modal"
                                                        data-target="#UpdateUserModal" onClick={() => this.setUpdateUser('worker',dat)} className="hover-2 btn text-uppercase" >update</button>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                </section>
                <section className="banner-bottom-w3layouts py-lg-5 py-md-5 py-3">
                    <div className="container ">
                        <div className="flex margin ">
                            <div className="margin-left">
                                <div className="col-md-12 margin-bottom">
                                    <h2 className="title animate-box">Managers List</h2>
                                </div>
                                {this.state.managementsList.length <= 0
                                    ? 'NO DB ENTRIES YET'
                                    : this.state.managementsList.map((dat) => (
                                        <div className="col-md-6 col-sm-6 ">
                                            <div className="feature-center animate-box min-width" data-animate-effect="fadeIn">
                                                <span className="icon">
                                                      {dat.image?(
                                                          <i><img className="image-profile image-profile image-profile-on-add" src={dat.image}/> </i>
                                                      ):( <i><img src="images/userIcon1.png"/> </i>)}
                                                </span>
                                                <h3>Name: {dat.fullName}</h3>
                                                <h3>Phone: {dat.phone}</h3>
                                                <h3>Mail: {dat.mail}</h3>
                                                <h3>Address: {dat.address}</h3>

                                                <Button
                                                    variant="contained"
                                                    color="secondary"
                                                    startIcon={<DeleteIcon />}
                                                    onClick={() => this.setState({
                                                        idUser: dat._id,
                                                        nameToDelete: dat.fullName,
                                                        itemToDelete: 'management'
                                                    })}
                                                    data-toggle="modal"
                                                    data-target="#deleteUserModal"
                                                >
                                                    Delete
                                                </Button>
                                                <button style={{margin: "5px"}} data-toggle="modal"
                                                        data-target="#UpdateUserModal" onClick={() => this.setUpdateUser('management',dat)} className="hover-2 btn text-uppercase" >update</button>
                                            </div>
                                        </div>
                                    ))}
                            </div>
                        </div>
                    </div>
                </section>
                <div className="modal fade" id="deleteUserModal" tabIndex="-1" role="dialog"
                     aria-labelledby="signUpModalLabel" aria-hidden="true">
                    <div className="modal-dialog margin-top" role="document">
                        <div className="modal-content">
                            <div className="modal-header ">
                                <h5 className="modal-title">Delete User</h5>
                                <button type="button" className="close" data-dismiss="modal"
                                        aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form id="deleteUsertmodalForm">
                                    <div className="form-group">
                                        <label htmlFor="name"
                                               className="col-form-label">Are you sure you want to
                                            delete the {this.state.itemToDelete} {this.state.nameToDelete}?</label>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary"
                                                data-dismiss="modal">cancel
                                        </button>
                                        <Button
                                            variant="contained"
                                            color="secondary"
                                            startIcon={<DeleteIcon />}
                                            onClick={() => this.deleteUser()}
                                            data-dismiss="modal"
                                        >
                                            Delete
                                        </Button>
                                        <Tooltip title="Delete from DB">
                                            <IconButton aria-label="delete" data-dismiss="modal" onClick={()=>this.deleteUser()}>
                                                <DeleteForeverIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="UpdateUserModal" tabIndex="-1" role="dialog"
                     aria-labelledby="signUpModalLabel" aria-hidden="true">
                    <div className="modal-dialog margin-top2" role="document">
                        <div className="modal-content">
                            <div className="modal-header ">
                                <h5 className="modal-title">Update User</h5>
                                <button type="button" className="close" data-dismiss="modal"
                                        aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div>
                                    <div className="form-group">
                                        <TextField id="outlined-basic"
                                                   placeholder='Enter name' label="Name" variant="outlined" value={this.state.nameToUpdate}
                                                   onChange={(e) => this.setState({nameToUpdate: e.target.value})}/>
                                    </div>
                                    <div className="form-group">
                                        <TextField id="outlined-basic"
                                                   placeholder='Enter phone number' label="Phone Number" variant="outlined" value={this.state.phoneToUpdate}
                                                   onChange={(e) => this.setState({phoneToUpdate: e.target.value})}/>
                                    </div>
                                    <div className="form-group">
                                        <TextField id="outlined-basic"
                                                   placeholder='Enter mail' label="Mail" variant="outlined" value={this.state.mailToUpdate}
                                                   onChange={(e) => this.setState({mailToUpdate: e.target.value})}/>
                                    </div>
                                    <div className="form-group">
                                        <TextField id="outlined-basic"
                                                   placeholder='Enter address' label="address" variant="outlined" value={this.state.addressToUpdate}
                                                   onChange={(e) => this.setState({addressToUpdate: e.target.value})}/>
                                    </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary"
                                                data-dismiss="modal">cancel
                                        </button>
                                        <input className="btn btn-primary" data-dismiss="modal"
                                               id="logoutBtnn" type="submit"
                                               value="Update"
                                               onClick={() => this.updateUser()}/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default ManageUsers;
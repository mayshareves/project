import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import Avatar from "@material-ui/core/Avatar";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Icon from '@material-ui/core/Icon';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import './header.css';
import {Input} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import MenuItem from '@material-ui/core/MenuItem';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import {DropzoneDialogBase} from "material-ui-dropzone";
let crypto = require('crypto');
const CLIENT_ID = '480951390654-7fk02972dfo8qh1pgpt7rr1eiclm52s3.apps.googleusercontent.com';

class Header extends Component {

    state = {
        data: [],
        idCart: null,
        carts: [],
        managements: [],
        id: 0,
        message: null,
        intervalIsSet: false,
        idToDelete: null,
        idToUpdate: null,
        objectToUpdate: null,
        userId: null,
        name: null,
        username: null,
        password:null,
        confirmedpassword:null,
        permission: null,
        login: null,
        mail: '',
        phone: '',
        address: '',
        personalName: '',
        familyName: '',
        pswSecret: null,
        showPassword: false,
        showConfirmedPassword: false,
        items: [],
        ordersHistory: [],
        workers: [],
        totalPrice: '',
        isLoginedUserGoogle: false,
        accessTokenUserGoogle: '',
        imageToAdd: '',
        fileObjects: [],
        open: false
    };

    componentDidMount() {
        this.getPswKey();
        this.refreshData();
    }

    componentWillUnmount() {
        if (this.state.intervalIsSet) {
            clearInterval(this.state.intervalIsSet);
            this.setState({ intervalIsSet: null });
        }
    }

    getDataFromDb = () => {
        fetch('http://localhost:3001/api/getData')
            .then((data) => data.json())
            .then((res) => this.setState({ data: res.data }));
    };

    getCustomerDataFromDB=()=>{
        fetch('http://localhost:3001/api/getCustomerData')
            .then((data) => data.json())
            .then((res) => this.setState({ data: res.data }));
    }

    getWorkersFromDB=()=>{
        fetch('http://localhost:3001/api/getWorkers')
            .then((data) => data.json())
            .then((res) => this.setState({ workers: res.data }));
    }

    refreshData=()=>{
        this.getCustomerDataFromDB();
        this.getMngFromDB();
        this.getWorkersFromDB();
    }

    getMngFromDB=()=>{
        fetch('http://localhost:3001/api/getManagementData')
            .then((data) => data.json())
            .then((res) => this.setState({ managements: res.data }));
    }

    getCartsFromDB=()=>{
        fetch('http://localhost:3001/api/getShoppingCartsData')
            .then((data) => data.json())
            .then((res) => this.setState({ carts: res.data }));
    }

    getPswKey=()=>{
        fetch('http://localhost:3001/api/getpsw')
            .then((data) => data.json())
            .then((res) => this.setState({ pswSecret: res.data[0]['pswSecret'] }));
    }


    refreshCartsAndItems = () => {
        fetch('http://localhost:3001/api/getShoppingCartsData')
            .then((data) => data.json())
            .then((res) => {
                    res.data.forEach((datCart) => {
                        if (datCart.idCustomer === this.state.userId) {
                            this.calculateTotalPrice(datCart.items);
                            this.setState({items: datCart.items});
                        }
                    });
                }
            );
    }

    putDataToDB = (message) => {
        let currentIds = this.state.data.map((data) => data.id);
        let idToBeAdded = 0;
        while (currentIds.includes(idToBeAdded)) {
            ++idToBeAdded;
        }
        console.log(idToBeAdded);
        axios.post('http://localhost:3001/api/putData', {
            id: idToBeAdded,
            message: message,
        });
    };

    putCustomerDataToDB = () => {
        let fullname = this.state.personalName + ' ' + this.state.familyName;
        let key = crypto.createCipher('aes-128-cbc', this.state.pswSecret);
        key.update(this.state.password, 'utf8');
        let encPsw = key.final('hex');
        if(this.state.permission==='customer'){
            axios.post('http://localhost:3001/api/putNewClient', {
                name: fullname,
                username:this.state.username,
                password:encPsw,
                mail: this.state.mail,
                phone: this.state.phone,
                address: this.state.address,
                image: this.state.imageToAdd
            }).then(()=>this.setState({imageToAdd: null }));
        } else if(this.state.permission==='management'){
                axios.post('http://localhost:3001/api/putNewManagement', {
                    name: fullname,
                    username:this.state.username,
                    password:encPsw,
                    mail: this.state.mail,
                    phone: this.state.phone,
                    address: this.state.address,
                    image: this.state.imageToAdd
                }).then(()=>this.setState({imageToAdd: null }));
        } else if(this.state.permission==='worker'){
            axios.post('http://localhost:3001/api/putNewWorker', {
                name: fullname,
                username:this.state.username,
                password:encPsw,
                mail: this.state.mail,
                phone: this.state.phone,
                address: this.state.address,
                image: this.state.imageToAdd
            }).then(()=>this.setState({imageToAdd: null }));
        }
//  TODO open modal
    };

    pay=()=> {
        if (this.state.items.length > 0) {
            //remove the basket to orderHistory
            let order = [];
            this.state.data.forEach((dat) => {
                if (dat._id === this.state.userId) {
                    order = dat.ordersHistory;
                    console.log(order);
                }
            });
            order[order.length] = {id: order.length,date: new Date(), items: this.state.items, totalPrice: this.state.totalPrice};
            axios.post('http://localhost:3001/api/putClientLogin', {
                id: this.state.userId,
                update: {ordersHistory: order}
            });
            let idCart;
            this.state.carts.forEach((datCart) => {
                if (datCart.idCustomer === this.state.userId) {
                    idCart = datCart._id;
                }
            });
            axios.post('http://localhost:3001/api/updateCartItems', {
                id: idCart,
                update: {items: []}
            });
            this.setState({items: []});
        }
    }

    logout = () => {
        if(this.state.login==='customer'){
            axios.post('http://localhost:3001/api/putClientLogin', {
                id: this.state.userId,
                update: {isConnected: false}
            });
            this.setState({ login: null });
            this.logoutGoogle();
        }else if(this.state.login==='management'){
            axios.post('http://localhost:3001/api/putmanagementLogin', {
                id: this.state.userId,
                update: {isConnected: false}
            });
            this.setState({ login: null });
        } else if(this.state.login==='worker'){
            axios.post('http://localhost:3001/api/putworkerLogin', {
                id: this.state.userId,
                update: {isConnected: false}
            });
            this.setState({ login: null });
        }
    };

    login = () => {
        let secret = crypto.createCipher('aes-128-cbc', this.state.pswSecret);
        secret.update(this.state.password, 'utf8');
        let encPsw = secret.final('hex');
        axios.post('http://localhost:3001/api/authenticateUser',{
            userName: this.state.username,
            password: encPsw
        }).then((res) => {
                const {exists, permission, userId} = res.data;
                if (exists) {
                    this.setState({userId: userId, login: permission});
                    if (permission === 'customer') {
                        axios.post('http://localhost:3001/api/putClientLogin', {
                            id: userId,
                            update: {isConnected: true}
                        });
                        //check if the cart for this customer already exists
                        let cartExists = false;
                        this.state.carts.forEach((datCart) => {
                            if (datCart.idCustomer === userId) {
                                cartExists = true;
                                this.setState({items: datCart.items, idCart: datCart._id});
                            }
                        });
                        if (cartExists === false) {
                            //put new  cart
                            axios.post('http://localhost:3001/api/putBasket', {
                                idCustomer: userId,
                                items: []
                            }).then((res)=>{
                                console.log('idCart: ',res.data.id);
                                this.setState({idCart: res.data.id});
                            });
                        }
                    }
                    if (permission === 'worker') {
                        axios.post('http://localhost:3001/api/putworkerLogin', {
                            id: userId,
                            update: {isConnected: true}
                        });
                    }
                    if (permission === 'management') {
                        axios.post('http://localhost:3001/api/putmanagementLogin', {
                            id: userId,
                            update: {isConnected: true}
                        });
                    }
                }else{
                    console.log('userName or password are not correct');
                }
            }
        );
    }

    deleteItemInCart = (id, name) => {
        let updatedItems = [];
        for (let i = 0, j = 0; i < this.state.items.length; i++, j++) {
            if (this.state.items[i].id !== id) {
                updatedItems[j] = this.state.items[i];
            } else {
                j--;
            }
        }
        axios.post('http://localhost:3001/api/putNewItemToBassket', {
            id: this.state.idCart,
            update: {items: updatedItems}
        }).then(() => {
            this.calculateTotalPrice(updatedItems);
            this.setState({items: updatedItems});
        });

    }

    updateAmount = (amount, id) => {
        let updatedItems = this.state.items;
        for (let i = 0; i < updatedItems.length; i++) {
            if (updatedItems[i].id === id) {
                updatedItems[i].amount = amount;
                break;
            }
        }
        axios.post('http://localhost:3001/api/putNewItemToBassket', {
            id: this.state.idCart,
            update: {items: updatedItems}
        }).then(() => {
            this.calculateTotalPrice(updatedItems);
            this.setState({items: updatedItems});
        });

    }

    calculateTotalPrice=(items)=>{
        let totalPrice = 0;
        items.forEach((item)=>{
            totalPrice+= item.amount * item.price.substring(1);
            }
        )
        this.setState({totalPrice: totalPrice});
    }

    loginGoogle =(response)=> {
        var profile = response.getBasicProfile();
        if(response.accessToken){
            let signedUp = false;
            this.state.data.forEach((customer)=>{
                if(customer.typeLogin==='google' && customer.googleId===profile.getId()){
                    signedUp = true;
                    this.setState({userId: customer._id, login: 'customer', username: customer.userName});
                }
            });
            if(signedUp===false){
                axios.post('http://localhost:3001/api/putNewClientGoogle', {
                    name: profile.getName(),
                    userName: profile.getName(),
                    mail: profile.getEmail(),
                    googleId: profile.getId(),
                    image: profile.getImageUrl(),
                    typeLogin: 'google'
                }).then((res)=>{
                    this.setState({userId: res.data.id, login: 'customer', username: profile.getName()});
                        axios.post('http://localhost:3001/api/putClientLogin', {
                            id: res.data.id,
                            update: {isConnected: true}
                        });
                        //check if the cart for this customer already exists
                        let cartExists = false;
                        this.state.carts.forEach((datCart) => {
                            if (datCart.idCustomer === res.data.id) {
                                cartExists = true;
                                this.setState({items: datCart.items});
                            }
                        });
                        if (cartExists === false) {
                            //put new  cart
                            axios.post('http://localhost:3001/api/putBasket', {
                                idCustomer: res.data.id,
                                items: []
                            });
                    }
                });
            }
            this.setState(state => ({
                isLoginedUserGoogle: true,
                accessTokenUserGoogle: response.accessToken,

            }));
        }
    }

    logoutGoogle= (response)=> {
        this.setState(state => ({
            isLoginedUserGoogle: false,
            accessTokenUserGoogle: ''
        }));
    }

    handleLoginFailure= (response) =>{
        alert('Failed to log in')
    }

    handleLogoutFailure= (response) =>{
        alert('Failed to log out')
    }

    onSaveImage=()=>{
        console.log('file: ', this.state.fileObjects);
        this.setState({imageToAdd: this.state.fileObjects[0].data })
    }

    render() {
        let login=this.state.login;
        if (!login)
        {
            return(
                <div className="banner ">
                <div className="container">
                    <div className="top-banner-left">
                        <div className="top-nav">
                            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul>
                                    <li><Link to='/home' className="nav-link">Home</Link></li>
                                    <li><Link to='/about' className="nav-link">About</Link></li>
                                    <li><Link to='/contact' className="nav-link">Contact</Link></li>
                                </ul>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div className="top-banner-right">
                        <div className="top-nav"><ul>

                            <li ><div data-toggle="modal" id = "loginbtn" data-target="#loginModal" >login</div></li>
                            <div className="modal fade" id="loginModal" tabIndex="-1" role="dialog"
                                 aria-labelledby="loginModalLabel" aria-hidden="true">
                                <div className="modal-dialog margin-top" role="document">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title" id="loginModalLabel">LOGIN</h5>
                                            <button type="button" className="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div className="modal-body">
                                            <form id="modalForm">
                                                <div className="form-group">
                                                    <label htmlFor="userName" className="col-form-label">User
                                                        Name:</label>
                                                    <input type="text" className="form-control" id="userName"
                                                           name='username' placeholder='Enter username'
                                                           required onChange={(e) => this.setState({username: e.target.value})}/>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="password"
                                                           className="col-form-label">Password:</label>
                                                    <input onChange={(e) => this.setState({password: e.target.value})}
                                                           type="password" className="form-control"
                                                           id="password" name='password'
                                                           placeholder='enter password' required/>
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="button" className="btn btn-secondary"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                    <input className="btn btn-primary" data-dismiss="modal"
                                                           id="loginBtnn" type="submit"
                                                           value="Login" onClick={() => this.login()}/>

                                                </div>
                                                <div data-dismiss="modal">
                                                    { this.state.isLoginedUserGoogle ?
                                                        <GoogleLogout
                                                            clientId={ CLIENT_ID }
                                                            buttonText='Logout'
                                                            onLogoutSuccess={ this.logoutGoogle }
                                                            onFailure={ this.handleLogoutFailure }
                                                        >
                                                        </GoogleLogout>:
                                                        <GoogleLogin
                                                            className="googleBtn"
                                                            clientId={ CLIENT_ID }
                                                            buttonText='Login with Google'
                                                            onSuccess={ this.loginGoogle }
                                                            onFailure={ this.handleLoginFailure }
                                                            cookiePolicy={ 'single_host_origin' }
                                                            responseType='code,token'
                                                        />
                                                    }
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <li><div data-toggle="modal" id = "signupbtn" data-target="#signUpModal">sign up</div></li>
                            <div className="Absolute-Center is-Responsive">
                                <div className="col-sm-12 col-md-10 col-md-offset-1">
                                    <div className="modal fade" id="signUpModal" tabIndex="-1" role="dialog"
                                         aria-labelledby="signUpModalLabel" aria-hidden="true">
                                        <div className="modal-dialog margin-top2" role="document">
                                            <div className="modal-content">
                                                <div className="modal-header ">
                                                    <h5 className="modal-title" id="signUpModalLabel">SIGNUP</h5>
                                                    <button type="button" className="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div className="modal-body">
                                                    <form >
                                                        <div style={{marginLeft: "35%"}}>
                                                            {this.state.imageToAdd?(
                                                                <i><img className="image-profile image-profile-on-add" src={this.state.imageToAdd}/> </i>
                                                            ):( <i><img  style={{height: "120px", cursor: "pointer"}} src="images/addImageProfile.png" onClick={() =>  this.setState({open: true})}/> </i>)}
                                                            <DropzoneDialogBase
                                                                acceptedFiles={['image/*']}
                                                                fileObjects={this.state.fileObjects}
                                                                cancelButtonText={"cancel"}
                                                                submitButtonText={"submit"}
                                                                maxFileSize={5000000}
                                                                open={this.state.open}
                                                                onAdd={newFileObjs => {
                                                                    console.log('onAdd', newFileObjs);
                                                                    this.setState({fileObjects: [].concat(this.state.fileObjects, newFileObjs)});
                                                                }}
                                                                onDelete={deleteFileObj => {
                                                                    console.log('onDelete', deleteFileObj);
                                                                    let index = 0;
                                                                    for(let i=0;i<this.state.fileObjects.length;i++){
                                                                        if(this.state.fileObjects[i].data===deleteFileObj.data){
                                                                            index = i;
                                                                            break;
                                                                        }
                                                                    }
                                                                    this.state.fileObjects.splice(index,1);
                                                                }}
                                                                onClose={() => this.setState({open: false})}
                                                                onSave={() => {
                                                                    this.setState({open: false});
                                                                    this.onSaveImage();
                                                                }}
                                                                showPreviews={true}
                                                                showFileNamesInPreview={true}
                                                            />
                                                        </div>
                                                        <div style={{margin: "0px 0px 20px 0px"}}>
                                                            All the fields are required.
                                                        </div>
                                                        <TextField id="outlined-basic"
                                                                 placeholder='Enter personal name' label="Personal Name" variant="outlined"
                                                                  onChange={(e) => this.setState({personalName: e.target.value})}/>
                                                        <TextField id="outlined-basic" style={{margin: "0px 10px"}}
                                                                   placeholder='Enter family name' label="Family Name" variant="outlined"
                                                                   onChange={(e) => this.setState({familyName: e.target.value})}/>
                                                        <Grid container  alignItems="flex-end" style={{margin: "10px 0 0"}}>
                                                            <Grid item>
                                                                <Icon className="fa fa-envelope-square" />
                                                            </Grid>
                                                            <Grid item>
                                                                <TextField id="input-with-icon-grid" label="Email"
                                                                           onChange={(e) => this.setState({mail: e.target.value})}/>
                                                            </Grid>
                                                        </Grid>
                                                        <Grid container  alignItems="flex-end" style={{margin: "10px 0 0"}}>
                                                            <Grid item>
                                                                <Icon className="fa fa-mobile" />
                                                            </Grid>
                                                            <Grid item>
                                                                <TextField id="input-with-icon-grid" label="Phone"
                                                                           onChange={(e) => this.setState({phone: e.target.value})}/>
                                                            </Grid>
                                                        </Grid>

                                                        <Grid container  alignItems="flex-end" style={{margin: "10px 0 0"}}>
                                                            <Grid item>
                                                                <Icon className="fa fa-map-marker" />
                                                            </Grid>
                                                            <Grid item>
                                                                <TextField id="input-with-icon-grid" label="Address"
                                                                           onChange={(e) => this.setState({address: e.target.value})}/>
                                                            </Grid>
                                                        </Grid>

                                                        <TextField
                                                            style={{margin: "10px 0 0"}}
                                                            id="input-with-icon-textfield"
                                                            label="User Name"
                                                            onChange={(e) => this.setState({username: e.target.value})}
                                                            InputProps={{
                                                                startAdornment: (
                                                                    <InputAdornment position="start">
                                                                        <AccountCircle />
                                                                    </InputAdornment>
                                                                ),
                                                            }}
                                                        />

                                                        <FormControl style={{margin: "10px 0px 0px 15px", width: "120px"}}>
                                                            <InputLabel id="demo-simple-select-label">Permission</InputLabel>
                                                            <Select
                                                                labelId="demo-simple-select-label"
                                                                id="demo-simple-select"
                                                                onChange={(e) => this.setState({permission: e.target.value})}
                                                            >
                                                                <MenuItem value={'customer'}>Customer</MenuItem>
                                                                <MenuItem value={'worker'}>Worker</MenuItem>
                                                                <MenuItem value={'management'}>Manager</MenuItem>
                                                            </Select>
                                                        </FormControl>
                                                        <FormControl style={{margin: "10px 0"}}>
                                                            <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                                                            <Input
                                                                id="standard-adornment-password"
                                                                type={this.state.showPassword ? 'text' : 'password'}
                                                                onChange={(e) => this.setState({password: e.target.value})}
                                                                endAdornment={
                                                                    <InputAdornment position="end">
                                                                        <IconButton
                                                                            aria-label=" password visibility"
                                                                            onClick={() => this.setState({showPassword: !this.state.showPassword})}>
                                                                            {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                                                        </IconButton>
                                                                    </InputAdornment>
                                                                }
                                                            />
                                                        </FormControl>
                                                        <FormControl >
                                                            <InputLabel htmlFor="standard-adornment-password">Confirm Password</InputLabel>
                                                            <Input
                                                                id="standard-adornment-password"
                                                                type={this.state.showConfirmedPassword ? 'text' : 'password'}
                                                                onChange={(e) => this.setState({confirmedpassword: e.target.value})}
                                                                endAdornment={
                                                                    <InputAdornment position="end">
                                                                        <IconButton
                                                                            aria-label=" password visibility"
                                                                            onClick={() => this.setState({showConfirmedPassword: !this.state.showConfirmedPassword})}>
                                                                            {this.state.showConfirmedPassword ? <Visibility /> : <VisibilityOff />}
                                                                        </IconButton>
                                                                    </InputAdornment>
                                                                }
                                                            />
                                                        </FormControl>

                                                        <div className="modal-footer">
                                                            <button type="button" className="btn btn-secondary"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                            <input className="btn btn-primary" data-dismiss="modal"
                                                                   id="signupBtnn" type="submit"
                                                                   value="SignUp"
                                                                   onClick={() => this.putCustomerDataToDB()}/>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ul></div>
                    </div>
                    <div className="clearfix"></div>
                </div>
                    <div className="container">
                        <div className="logo-wthree text-center py-4" style={{textAlign: "center"}}>
                            <div className="navbar-brand" href="">
                                PhotoWall <span> Make Memories</span></div>
                        </div>
                    </div>
                </div>);
        }
        else {
            let userName = this.state.username;
            let userId = this.state.userId;

            return(
                <div className="banner ">
                    <div className="container">
                        <div className="top-banner-left">
                            <div className="top-nav">
                                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul>
                                        <li><Link to='/home' className="nav-link">Home</Link></li>
                                        <li><Link to='/about' className="nav-link">About</Link></li>
                                        <li><Link to='/contact' className="nav-link">Contact</Link></li>
                                        {this.state.login!=='customer'?(<li><Link to='/management_items' className="nav-link">Items Mng</Link></li>
                                            ):(<li><Link  className="nav-link"    to={{
                                            pathname: '/catalog',
                                            userName: {userName},
                                            userId: {userId}
                                        }}>Catalog</Link></li>)}
                                        {this.state.login=='management'?(<li><Link to='/management_users' className="nav-link">Users Mng</Link></li>
                                        ):(<li></li>)}
                                         {this.state.login=='customer'?(<li><Link to={{pathname: '/orders_history', userId: {userId}}} className="nav-link">Orders History</Link></li>
                                        ):(<div></div>)}
                                    </ul>
                                    <div className="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div className="top-banner-right">
                            <div className="top-nav"><ul>
                                {this.state.login=='customer'?(
                                    <li ><div data-toggle="modal" id="cartsbtn" data-target="#cartsModal"  onClick={()=>this.refreshCartsAndItems()}>
                                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" className="bi bi-cart3"
                                         fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                              d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                                    </svg></div></li>
                                ):(<div></div>)}

                                <div className="modal fade" id="cartsModal" tabIndex="-1" role="dialog"
                                     aria-labelledby="cartsModalLabel" aria-hidden="true">
                                    <div className="modal-dialog margin-top2" role="document">
                                        <div className="modal-content"  style={{width:"650px"}}>
                                            <div className="modal-header ">
                                                <h5 className="modal-title" id="cartsModalLabel">My Shoppings</h5>
                                                <button type="button" className="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>

                                            <div className="modal-body">
                                                <form id="cartsmodalForm">
                                                    <div
                                                        style={{
                                                            maxHeight: 400,
                                                            padding: 10,
                                                            overflow: "auto"
                                                        }}
                                                    >
                                                        <Table>
                                                            <TableHead>
                                                                <TableRow>
                                                                    <TableCell>Item name</TableCell>
                                                                    <TableCell>Category</TableCell>
                                                                    <TableCell>Price</TableCell>
                                                                    <TableCell>Amount</TableCell>
                                                                    <TableCell>Action</TableCell>
                                                                </TableRow>
                                                            </TableHead>
                                                            <TableBody>
                                                                {this.state.items.map((item,i) => {
                                                                    return  <TableRow>
                                                                        <TableCell>
                                                                            {item.data}
                                                                        </TableCell>
                                                                        <TableCell>
                                                                            {item.category}
                                                                        </TableCell>
                                                                        <TableCell>{item.price}</TableCell>
                                                                        <TableCell>
                                                                            <TextField
                                                                                type="number"
                                                                                style={{ width: 40 }}
                                                                                value={item.amount}
                                                                                onChange={(e) => this.updateAmount(e.target.value, item.id)}
                                                                            />
                                                                        </TableCell>
                                                                        <TableCell>
                                                                            <Button
                                                                                color="secondary"
                                                                                onClick={() => this.deleteItemInCart(item.id, item.data)}
                                                                            >
                                                                                Delete
                                                                            </Button>
                                                                        </TableCell>
                                                                    </TableRow>;
                                                                })}
                                                            </TableBody>
                                                        </Table>
                                                    </div>
                                                    <div className="modal-footer">
                                                        <div>
                                                            Total Price: ${this.state.totalPrice}
                                                        </div>
                                                        <button type="button" className="btn btn-secondary"
                                                                data-dismiss="modal">cancel
                                                        </button>
                                                        <input className="btn btn-primary" data-dismiss="modal"
                                                               id="payBtnn" type="submit"
                                                               value="Payment"
                                                               onClick={() => this.pay()}/>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {this.state.login=='customer'?(<li ><Link to={{pathname: '/blog_chat/join', userName: this.state.username, userId: this.state.userId}} className="nav-link" id="message">
                                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16"
                                         className="bi bi-chat-right-text" fill="currentColor"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                              d="M2 1h12a1 1 0 0 1 1 1v11.586l-2-2A2 2 0 0 0 11.586 11H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1zm12-1a2 2 0 0 1 2 2v12.793a.5.5 0 0 1-.854.353l-2.853-2.853a1 1 0 0 0-.707-.293H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12z"/>
                                        <path fill-rule="evenodd"
                                              d="M3 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 6a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 6zm0 2.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z"/>
                                    </svg></Link></li>):(<div/>)}
                                <li>
                                    <Link to={{pathname: `/profil/${this.state.username}`, userId: this.state.userId,permission: this.state.login}}><Avatar></Avatar></Link>
                                </li>
                                <li>
                                    <Link to='/about' onClick={() => this.logout()} className="nav-link">log out</Link>
                                </li>
                            </ul>
                                <div className=" userName" id="hellowUser">hellow {this.state.username}</div>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                    <div className="container">
                        <div className="logo-wthree text-center py-4" style={{textAlign: "center"}}>
                            <div className="navbar-brand" href="">
                                PhotoWall <span> Make Memories</span></div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default Header;

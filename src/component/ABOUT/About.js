import React, {Component} from 'react';
import '../../containers/App.css';

class About extends Component{

    render() {
        return (
            <section className="banner-bottom-w3layouts py-lg-5 py-md-5 py-3">
                <div className="container">
                    <div className="inner-sec-w3ls py-lg-5 py-3">
                        <h2 className="title-wthree text-center mb-lg-5 mb-3">About Us</h2>
                        <div className="row">
                            <div className="col-lg-6 about-img" data-aos="flip-right">
                                <img className="img-fluid" src="images/ab.jpg" alt=""/>
                            </div>
                            <div className="col-lg-6 about-right mt-lg-4" data-aos="flip-left">
                                <h4 className="sub-tittle">Who We Are</h4>
                                <h3 className="tittle text-uppercase">Photo wall web shop since 1980</h3>
                                <p className="my-4">Lorem ipsum dolor sit amet Neque porro quisquam est qui dolorem Lorem
                                    int ipsum dolor sit amet when an unknown printer took a galley of type.Vivamus id tempor
                                    felis.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.Lorem
                                    ipsum dolor sit amet Neque porro quisquam est qui dolorem Lorem int ipsum dolor sit
                                    amet.</p>

                                <ul className="author d-flex">
                                    <li><img className="img-fluid" src="images/author.jpg" alt=""/></li>
                                    <li><span>Bob Remi</span>Photographer</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row mt-lg-5 stats-con">
                            <div className="col-sm-3 col-6 stats_info counter_grid" data-aos="fade-up">

                                <p className="counter">645</p>
                                <h4>Projects Done</h4>

                            </div>
                            <div className="col-sm-3 col-6 stats_info counter_grid1" data-aos="fade-up">

                                <p className="counter">563</p>
                                <h4>Satisfied Clients</h4>

                            </div>
                            <div className="col-sm-3 col-6 stats_info counter_grid" data-aos="fade-up">

                                <p className="counter">1145</p>
                                <h4>Awards</h4>

                            </div>
                            <div className="col-sm-3 col-6 stats_info counter_grid2" data-aos="fade-up">

                                <p className="counter">1045</p>
                                <h4>Happy Clients</h4>

                            </div>
                        </div>
                        <p className="my-4" data-aos="fade-up">Lorem ipsum dolor sit amet Neque porro quisquam est qui
                            dolorem Lorem int ipsum dolor sit amet when an unknown printer took a galley of type.Vivamus id
                            tempor felis.Lorem ipsum dolor sit amet Neque porro quisquam est qui dolorem Lorem int ipsum
                            dolor sit amet when an unknown printer took a galley of type.Vivamus id tempor felis.Lorem ipsum
                            dolor sit amet Neque porro quisquam est qui dolorem Lorem int ipsum dolor sit amet when an
                            unknown printer took a galley of type.Vivamus id tempor felis.Simply dummy text of the printing
                            and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s.Lorem ipsum dolor sit amet Neque porro quisquam est qui dolorem Lorem int ipsum dolor sit
                            amet.</p>
                    </div>
                </div>
            </section>


        );
    }
}

export default About;

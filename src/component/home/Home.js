import React, {Component} from 'react'
// import {Link} from 'react-router-dom';
import '../../containers/App.css';

class Home extends Component{

render() {
    return (
        <section className="content-main-w3" id="home">
             <div className="container">
            <div className="header_top_w3ls">
            <div className="container-fluid gallery-lightbox my-5">
                <div className="row m-0">
                    <div className="col-lg-2 col-md-2 col-sm-3 p-0 snap-img">
                        <div className="gallery_grid1 hover08" data-aos="fade-up">
                            <div className="gallery_effect">
                                <a href="images/1.jpg" data-lightbox="example-set"
                                   data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">
                                    <figure><img src="images/1.jpg" alt=" " className="img-fluid"/></figure>
                                </a>
                            </div>
                        </div>
                        {/*<div className="gallery_grid1 hover08" data-aos="fade-up">*/}
                        {/*    <div className="gallery_effect">*/}
                        {/*        <a href="images/16.jpg" data-lightbox="example-set"*/}
                        {/*           data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                        {/*            <figure><img src="images/16.jpg" alt=" " className="img-fluid"/></figure>*/}
                        {/*        </a>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        {/*<div className="gallery_grid1 hover08" data-aos="fade-up">*/}
                        {/*    <div className="gallery_effect">*/}
                        {/*        <a href="images/2.jpg" data-lightbox="example-set"*/}
                        {/*           data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                        {/*            <figure><img src="images/2.jpg" alt=" " className="img-fluid"/></figure>*/}
                        {/*        </a>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        <div className="gallery_grid1 hover08" data-aos="fade-up">
                            <div className="gallery_effect">
                                <a href="images/17.jpg" data-lightbox="example-set"
                                   data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">
                                    <figure><img src="images/17.jpg" alt=" " className="img-fluid"/></figure>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2 col-md-2 col-sm-3 p-0 snap-img">
                        <div className="gallery_grid1 hover08" data-aos="fade-up">
                            <div className="gallery_effect">
                                <a href="images/3.jpg" data-lightbox="example-set"
                                   data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">
                                    <figure><img src="images/3.jpg" alt=" " className="img-fluid"/></figure>
                                </a>
                            </div>
                        </div>
                        <div className="gallery_grid1 hover08" data-aos="fade-up">
                            <div className="gallery_effect">
                                <a href="images/4.jpg" data-lightbox="example-set"
                                   data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">
                                    <figure><img src="images/4.jpg" alt=" " className="img-fluid"/></figure>
                                </a>
                            </div>
                        </div>
                        <div className="gallery_grid1 hover08" data-aos="fade-up">
                            <div className="gallery_effect">
                                <a href="images/18.jpg" data-lightbox="example-set"
                                   data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">
                                    <figure><img src="images/18.jpg" alt=" " className="img-fluid"/></figure>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2 col-md-2 col-sm-3 p-0 snap-img">
                        <div className="gallery_grid1 hover08">
                            <div className="gallery_effect" data-aos="fade-up">
                                <a href="images/5.jpg" data-lightbox="example-set"
                                   data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">
                                    <figure><img src="images/5.jpg" alt=" " className="img-fluid"/></figure>
                                </a>
                            </div>
                            {/*<div className="gallery_effect" data-aos="fade-up">*/}
                            {/*    <a href="images/9.jpg" data-lightbox="example-set"*/}
                            {/*       data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                            {/*        <figure><img src="images/9.jpg" alt=" " className="img-fluid"/></figure>*/}
                            {/*    </a>*/}
                            {/*</div>*/}
                        </div>
                        <div className="gallery_grid1 hover08" data-aos="fade-up">
                            <div className="gallery_effect">
                                <a href="images/19.jpg" data-lightbox="example-set"
                                   data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">
                                    <figure><img src="images/19.jpg" alt=" " className="img-fluid"/></figure>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2 col-md-2 col-sm-3 p-0 snap-img">
                        {/*<div className="gallery_grid1 hover08" data-aos="fade-up">*/}
                        {/*    <div className="gallery_effect">*/}
                        {/*        <a href="images/6.jpg" data-lightbox="example-set"*/}
                        {/*           data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                        {/*            <figure><img src="images/6.jpg" alt=" " className="img-fluid"/></figure>*/}
                        {/*        </a>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        <div className="gallery_grid1 hover08">
                            {/*<div className="gallery_effect" data-aos="fade-up">*/}
                            {/*    <a href="images/7.jpg" data-lightbox="example-set"*/}
                            {/*       data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                            {/*        <figure><img src="images/7.jpg" alt=" " className="img-fluid"/></figure>*/}
                            {/*    </a>*/}
                            {/*</div>*/}
                            {/*<div className="gallery_effect" data-aos="fade-up">*/}
                            {/*    <a href="images/8.jpg" data-lightbox="example-set"*/}
                            {/*       data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                            {/*        <figure><img src="images/8.jpg" alt=" " className="img-fluid"/></figure>*/}
                            {/*    </a>*/}
                            {/*</div>*/}
                        </div>
                        <div className="gallery_grid1 hover08" data-aos="fade-up">
                            {/*<div className="gallery_effect">*/}
                            {/*    <a href="images/20.jpg" data-lightbox="example-set"*/}
                            {/*       data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                            {/*        <figure><img src="images/20.jpg" alt=" " className="img-fluid"/></figure>*/}
                            {/*    </a>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                    {/*<div className="col-lg-2 col-md-2 p-0 snap-img">*/}
                        {/*<div className="gallery_grid1 hover08">*/}
                        {/*    <div className="gallery_effect" data-aos="fade-up">*/}
                        {/*        <a href="images/10.jpg" data-lightbox="example-set"*/}
                        {/*           data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                        {/*            <figure><img src="images/10.jpg" alt=" " className="img-fluid"/></figure>*/}
                        {/*        </a>*/}
                        {/*    </div>*/}

                        {/*</div>*/}

                        {/*<div className="gallery_grid1 hover08">*/}
                        {/*    <div className="gallery_effect" data-aos="fade-up">*/}
                        {/*        <a href="images/12.jpg" data-lightbox="example-set"*/}
                        {/*           data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                        {/*            <figure><img src="images/12.jpg" alt=" " className="img-fluid"/></figure>*/}
                        {/*        </a>*/}
                        {/*    </div>*/}

                        {/*</div>*/}
                        {/*<div className="gallery_grid1 hover08">*/}
                        {/*    <div className="gallery_effect" data-aos="fade-up">*/}
                        {/*        <a href="images/21.jpg" data-lightbox="example-set"*/}
                        {/*           data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                        {/*            <figure><img src="images/21.jpg" alt=" " className="img-fluid"/></figure>*/}
                        {/*        </a>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                    {/*</div>*/}


                    <div className="col-lg-2 col-md-2 p-0 snap-img">
                        {/*<div className="gallery_grid1 hover08">*/}
                        {/*    <div className="gallery_effect" data-aos="fade-up">*/}
                        {/*        <a href="images/13.jpg" data-lightbox="example-set"*/}
                        {/*           data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                        {/*            <figure><img src="images/13.jpg" alt=" " className="img-fluid"/></figure>*/}
                        {/*        </a>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        {/*<div className="gallery_grid1 hover08">*/}
                        {/*    <div className="gallery_effect" data-aos="fade-up">*/}
                        {/*        <a href="images/14.jpg" data-lightbox="example-set"*/}
                        {/*           data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                        {/*            <figure><img src="images/14.jpg" alt=" " className="img-fluid"/></figure>*/}
                        {/*        </a>*/}
                        {/*    </div>*/}

                        {/*</div>*/}
                        {/*<div className="gallery_grid1 hover08">*/}
                        {/*    <div className="gallery_effect" data-aos="fade-up">*/}
                        {/*        <a href="images/15.jpg" data-lightbox="example-set"*/}
                        {/*           data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">*/}
                        {/*            <figure><img src="images/15.jpg" alt=" " className="img-fluid"/></figure>*/}
                        {/*        </a>*/}
                        {/*    </div>*/}

                        {/*</div>*/}
                        <div className="gallery_grid1 hover08">
                            <div className="gallery_effect" data-aos="fade-up">
                                <a href="images/22.jpg" data-lightbox="example-set"
                                   data-title-wthree="Lorem Ipsum is simply dummy the when an unknown galley of type and scrambled it to make a type specimen.">
                                    <figure><img src="images/22.jpg" alt=" " className="img-fluid"/></figure>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


                </div>

            </div>
             </div>
        </section>


    );
}
}



export default Home;

import React, {Component} from 'react'
import axios from 'axios';
import TextField from "@material-ui/core/TextField";

class ManageItems extends  Component{

    state={
        albumList: [],
        packageList: [],
        photographerList: [],
        photoList: [],
        intervalIsSet: false,
        idItem: null,
        itemToDelete: null,
        nameToDelete: null,
        itemToUpdate: null,
        typeToUpdate:null,
        nameToUpdate: null,
        costToUpdate: null,
        numHoursToUpdate: null,
        numPhotographerToUpdate: null,
        placeToUpdate: null,
        typePhotosToUpdate: null,
        idToUpdate: null,
        phoneNumberToUpdate: null,
        numPageToUpdate: null,
        photosNumberToUpdate: null,
        sizePageToUpdate: null,
        categoryToUpdate: null,
        sizeToUpdate: null

    };

    componentDidMount() {
      this.getAlbumData();
      this.getPackageData();
      this.getPhotoData();
      this.getPhotographerData();
    }

    componentWillUnmount() {
        if (this.state.intervalIsSet) {
            clearInterval(this.state.intervalIsSet);
            this.setState({ intervalIsSet: null });
        }
    }

    getAlbumData=()=>{
        fetch('http://localhost:3001/api/getAlbumData')
            .then((data) => data.json())
            .then((res) => {
                const albumData=[];
                res.data.map((dat)=>{
                    if(dat.valid===true) {
                        albumData[albumData.length] = dat;
                    }
                })
                this.setState({ albumList: albumData })});
    }

    getPackageData=()=>{
        fetch('http://localhost:3001/api/getPackages')
            .then((data) => data.json())
            .then((res) =>{
                const packageData=[];
                res.data.map((dat)=>{
                    if(dat.valid===true) {
                        packageData[packageData.length] = dat;
                    }
                })
                this.setState({ packageList: packageData })
            } );
    }

    getPhotographerData=()=>{
        fetch('http://localhost:3001/api/getPhotograghersData')
            .then((data) => data.json())
            .then((res) =>
            {
                const photographerData=[];
                res.data.map((dat)=>{
                    if(dat.valid===true){
                        photographerData[photographerData.length]=dat;
                    }
                })
                this.setState({ photographerList: photographerData })});
    }

    getPhotoData=()=>{
        fetch('http://localhost:3001/api/getPhotoData')
            .then((data) => data.json())
            .then((res) => {
                const photoData=[];
                res.data.map((dat)=>{
                    if(dat.valid===true){
                        photoData[photoData.length]=dat;
                    }
                })
                this.setState({ photoList: photoData })});
    }

    deleteAlbum=()=>{
        axios.post('http://localhost:3001/api/deleteAlbumItem', {
            id: this.state.idItem,
            update: { valid: 'false' },
        }).then(res => {
            this.getAlbumData();
        })
            .catch((err) => {
                console.log(err);
            });
    }

    deletePackage=()=>{
        axios.post('http://localhost:3001/api/deletePackageItem', {
            id: this.state.idItem,
            update: { valid: 'false' },

        }).then(res => {
            this.getPackageData();
        })
            .catch((err) => {
                console.log(err);
            });
    }

    deletePhotographer = () => {
        axios.post('http://localhost:3001/api/deletePhotographerItem', {
            id: this.state.idItem,
            update: {valid: 'false'},
        }).then(res => {
            this.getPhotographerData();
        })
        .catch((err) => {
            console.log(err);
        });
    }

    deletePhoto=()=>{
        axios.post('http://localhost:3001/api/deletePhotoItem', {
            id: this.state.idItem,
            update: { valid: 'false' },
        }).then(res => {
            this.getPhotoData();
        })
            .catch((err) => {
                console.log(err);
            });
    }

    deleteItem = () => {
        switch (this.state.itemToDelete) {
            case 'package':
                this.deletePackage();
                break;
            case 'photographer':
                this.deletePhotographer();
                break;
            case 'album':
                this.deleteAlbum();
                break;
            case 'photo':
                this.deletePhoto();
                break;
            default:
            // code block
        }
    }

    updatePackage = ()=>{
        axios.post('http://localhost:3001/api/deletePackageItem', {
            id: this.state.idToUpdate,
            update: { type: this.state.nameToUpdate, cost: this.state.costToUpdate,
                numHours: this.state.numHoursToUpdate, numPhotogragher: this.state.numPhotographerToUpdate, place: this.state.placeToUpdate,typePhotos: this.state.typePhotosToUpdate},

        }).then(res => {
            this.getPackageData();
        })
            .catch((err) => {
                console.log(err);
            });
    }
    updatePhotographer = ()=>{
        axios.post('http://localhost:3001/api/deletePhotographerItem', {
            id: this.state.idToUpdate,
            update: { name: this.state.nameToUpdate, cost: this.state.costToUpdate,
                phoneNumber: this.state.phoneNumberToUpdate},

        }).then(res => {
            this.getPhotographerData();
        })
            .catch((err) => {
                console.log(err);
            });
    }

    updateAlbum= ()=>{
        axios.post('http://localhost:3001/api/deleteAlbumItem', {
            id: this.state.idToUpdate,
            update: { name: this.state.nameToUpdate, cost: this.state.costToUpdate,
                numPage: this.state.numPagesToUpdate,photosNumber: this.state.photosNumberToUpdate, sizePage: this.state.sizePageToUpdate},

        }).then(res => {
            this.getAlbumData();
        })
            .catch((err) => {
                console.log(err);
            });
    }

    updatePhoto= ()=>{
        axios.post('http://localhost:3001/api/deletePhotoItem', {
            id: this.state.idToUpdate,
            update: { name: this.state.nameToUpdate, cost: this.state.costToUpdate,
                category: this.state.categoryToUpdate,size: this.state.sizeToUpdate},

        }).then(res => {
            this.getPhotoData();
        })
            .catch((err) => {
                console.log(err);
            });
    }

    updateItem = ()=>{
        switch (this.state.typeToUpdate) {
            case 'package':
                this.updatePackage();
                break;
            case 'photographer':
                this.updatePhotographer();
                break;
            case 'album':
                this.updateAlbum();
                break;
            case 'photo':
                this.updatePhoto();
                break;
            default:
            // code block
        }
    }

    setUpdateItem = (typeToUpdate, itemToUpdate)=>{
        switch (typeToUpdate) {
            case 'package':
                this.setState({
                nameToUpdate: itemToUpdate.type,
                costToUpdate: itemToUpdate.cost,
                numHoursToUpdate: itemToUpdate.numHours,
                numPhotographerToUpdate: itemToUpdate.numPhotogragher,
                placeToUpdate: itemToUpdate.place,
                typePhotosToUpdate: itemToUpdate.typePhotos,
                idToUpdate: itemToUpdate._id, typeToUpdate: typeToUpdate});
                break;
            case 'photographer':
                this.setState({
                    costToUpdate: itemToUpdate.cost,
                    nameToUpdate: itemToUpdate.name,
                    phoneNumberToUpdate: itemToUpdate.phoneNumber,
                    idToUpdate: itemToUpdate._id, typeToUpdate: typeToUpdate
                });
                break;
            case 'album':
                this.setState({
                    costToUpdate: itemToUpdate.cost,
                    nameToUpdate: itemToUpdate.name,
                    numPagesToUpdate: itemToUpdate.numPage,
                    photosNumberToUpdate: itemToUpdate.photosNumber,
                    sizePageToUpdate: itemToUpdate.sizePage,
                    idToUpdate: itemToUpdate._id, typeToUpdate: typeToUpdate
                });
                break;
            case 'photo':
                this.setState({
                    costToUpdate: itemToUpdate.cost,
                    nameToUpdate: itemToUpdate.name,
                    categoryToUpdate: itemToUpdate.category,
                    sizeToUpdate: itemToUpdate.size,
                    idToUpdate: itemToUpdate._id, typeToUpdate: typeToUpdate
                });
                break;
            default:
            // code block
        }
    }

    render() {
        return (
            <div>
                <section class="team-main text-center py-5 py-3">
                    <div class="container">
                        <div class="inner-sec-w3ls py-lg-5 py-3">
                            <h2 class="title-wthree text-center mb-lg-5 mb-3">Affordable packages</h2>
                            <div class="row t-in mt-3">
                                {this.state.packageList.length <= 0
                                    ? 'NO DB ENTRIES YET'
                                    : this.state.packageList.map((dat) => (
                                        <div className="col-md-4 price-main-info text-center" data-aos="fade-up">
                                            <div className="price-inner card box-shadow">
                                                <div className="card-body">
                                                    <h4 className="text-uppercase mb-3">{dat.type}</h4>
                                                    <h5 className="card-title-wthree pricing-card-title-wthree">
                                                        <span className="align-top"></span>{dat.cost}
                                                    </h5>
                                                    <ul className="list-unstyled mt-3 mb-4">
                                                        <li>{dat.numPhotogragher} Photographs</li>
                                                        <li>{dat.numHours} Hour</li>
                                                        <li>{dat.typePhotos}</li>
                                                        <li>{dat.place}</li>
                                                    </ul>
                                                    <button onClick={() => this.setState({itemToDelete: 'package', idItem: dat._id, nameToDelete:dat.type})} className="hover-2 btn text-uppercase" data-toggle="modal"   data-target="#deleteItemModal">delete</button>
                                                    <button onClick={() => this.setUpdateItem('package', dat)} style={{margin: "5px"}} className="hover-2 btn text-uppercase" data-toggle="modal"   data-target="#updateItemModal">update</button>
                                                </div>
                                            </div>
                                        </div>
                                    ))}

                            </div>

                        </div>
                    </div>
                </section>
                <section class="team-main text-center py-5 py-3">
                    <div class="container">
                        <div class="inner-sec-w3ls py-lg-5 py-3">
                            <h2 class="title-wthree text-center mb-lg-5 mb-3">Our Photographers</h2>
                            <div class="row mt-lg-5 mt-3">
                                {this.state.photographerList.length <= 0
                                    ? 'NO DB ENTRIES YET'
                                    : this.state.photographerList.map((dat) => (
                                        <div className="col-md-4 col-sm-6 team-gd " data-aos="zoom-in">
                                            <div className="box16">
                                                <img src={dat.image} alt=" " className="img-fluid"/>
                                                <div className="box-content">
                                                    <h3 className="title-wthree">{dat.name}</h3>
                                                    <div> <span className="post">cost: {dat.cost} per hour</span></div>
                                                    <div> <span className="post">phone: {dat.phoneNumber}</span>
                                                    </div>
                                                </div>
                                                <button onClick={() => this.setState({itemToDelete: 'photographer', idItem: dat._id, nameToDelete:dat.name})} className="hover-2 btn text-uppercase" data-toggle="modal"   data-target="#deleteItemModal">delete</button>
                                                <button onClick={() => this.setUpdateItem('photographer',dat)} style={{margin: "5px"}} className="hover-2 btn text-uppercase" data-toggle="modal"   data-target="#updateItemModal">update</button>
                                            </div>
                                        </div>
                                    ))}
                            </div>
                        </div>
                    </div>
                </section>
                <section className="team-main text-center py-5 py-3">
                    <div className="container">
                        <div className="inner-sec-w3ls py-lg-5 py-3">
                            <h2 className="title-wthree text-center mb-lg-5 mb-3">Album Design</h2>
                            <div className="row mt-lg-5 mt-3">
                                {this.state.albumList.length <= 0
                                    ? 'NO DB ENTRIES YET'
                                    : this.state.albumList.map((dat) => (
                                        <div className="col-md-4 col-sm-6 team-gd " data-aos="zoom-in">
                                            <div className="box16">
                                                <img src={dat.image} alt=" " className="img-fluid"/>
                                                <div className="box-content">
                                                    <h3 className="title-wthree">{dat.name}</h3>
                                                    <div><span className="post">cost: {dat.cost}</span></div>
                                                    <div><span className="post">page size: {dat.sizePage}</span>
                                                    </div>
                                                    <div><span className="post">image number: {dat.photosNumber}</span></div>
                                                    <div><span className="post">pages number: {dat.numPage}</span></div>
                                                </div>
                                                <button onClick={() => this.setState({itemToDelete: 'album', idItem: dat._id, nameToDelete:dat.name})} className="hover-2 btn text-uppercase" data-toggle="modal"   data-target="#deleteItemModal">delete</button>
                                                <button onClick={() => this.setUpdateItem('album', dat)} style={{margin: "5px"}} className="hover-2 btn text-uppercase" data-toggle="modal"   data-target="#updateItemModal">update</button>
                                            </div>
                                        </div>
                                    ))}
                            </div>
                        </div>
                    </div>
                </section>
                <section className="team-main text-center py-5 py-3">
                    <div className="container">
                        <div className="inner-sec-w3ls py-lg-5 py-3">
                            <h2 className="title-wthree text-center mb-lg-5 mb-3">Photos</h2>
                            <div className="row mt-lg-5 mt-3">
                                {this.state.photoList.length <= 0
                                    ? 'NO DB ENTRIES YET'
                                    : this.state.photoList.map((dat) => (
                                        <div className="col-md-4 col-sm-6 team-gd aos-animate" data-aos="zoom-in">
                                            <div className="box16">
                                                <img src={dat.image} alt=" " className="img-fluid"/>
                                                <div className="box-content">
                                                    <h3 className="title-wthree">category: {dat.category}</h3>
                                                    <div><span className="post">cost: {dat.cost}</span></div>
                                                    <div><span className="post">size: {dat.size}</span>
                                                    </div>
                                                </div>
                                                <button onClick={() => this.setState({itemToDelete: 'photo', idItem: dat._id, nameToDelete:dat.name})} className="hover-2 btn text-uppercase" data-toggle="modal"   data-target="#deleteItemModal">delete</button>
                                                <button onClick={() => this.setUpdateItem('photo',dat)} style={{margin: "5px"}} className="hover-2 btn text-uppercase" data-toggle="modal"   data-target="#updateItemModal">update</button>
                                            </div>
                                        </div>
                                    ))}
                            </div>
                        </div>
                    </div>
                    <div className="modal fade" id="deleteItemModal" tabIndex="-1" role="dialog"
                         aria-labelledby="deleteItemModalLabel" aria-hidden="true">
                        <div className="modal-dialog margin-top" role="document">
                            <div className="modal-content">
                                <div className="modal-header ">
                                    <h5 className="modal-title">Delete {this.state.itemToDelete}</h5>
                                    <button type="button" className="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <form id="deleteUsertmodalForm">
                                        <div className="form-group">
                                            <label htmlFor="name"
                                                   className="col-form-label">Are you sure you want to delete the {this.state.itemToDelete} '{this.state.nameToDelete}'?</label>
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-secondary"
                                                    data-dismiss="modal">cancel
                                            </button>
                                            <input className="btn btn-primary" data-dismiss="modal"
                                                   type="submit"
                                                   value="Delete"
                                                   onClick={() => this.deleteItem()}/>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal fade" id="updateItemModal" tabIndex="-1" role="dialog"
                         aria-labelledby="updateItemModalLabel" aria-hidden="true">
                        <div className="modal-dialog margin-top2" role="document">
                            <div className="modal-content">
                                <div className="modal-header ">
                                    <h5 className="modal-title">Update {this.state.typeToUpdate}</h5>
                                    <button type="button" className="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <form >
                                        {this.state.typeToUpdate==='package' &&(
                                            <div>
                                                <div className="form-group">
                                                <TextField id="outlined-basic"
                                                           placeholder='Enter package name' label="Name" variant="outlined" value={this.state.nameToUpdate}
                                                           onChange={(e) => this.setState({nameToUpdate: e.target.value})}/>
                                                </div>
                                                <div className="form-group">
                                                <TextField
                                                    label="Cost"
                                                    value={this.state.costToUpdate}
                                                    onChange={(e) => this.setState({costToUpdate: e.target.value})}
                                                    name="numberformat"
                                                    id="formatted-numberformat-input"

                                                />
                                                </div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter photographers number' label="Photographers Number" variant="outlined" value={this.state.numPhotographerToUpdate}
                                                               onChange={(e) => this.setState({numPhotographerToUpdate: e.target.value})}/>
                                                </div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter hours number' label="Hours Number" variant="outlined" value={this.state.numHoursToUpdate}
                                                               onChange={(e) => this.setState({numHoursToUpdate: e.target.value})}/>
                                                </div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter photos type' label="Photos Type" variant="outlined" value={this.state.typePhotosToUpdate}
                                                               onChange={(e) => this.setState({typePhotosToUpdate: e.target.value})}/>
                                                </div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter place' label="Place" variant="outlined" value={this.state.placeToUpdate}
                                                               onChange={(e) => this.setState({placeToUpdate: e.target.value})}/>
                                                </div>
                                            </div>
                                        )}
                                        {this.state.typeToUpdate==='photographer' &&(
                                            <div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter photographer name' label="Name" variant="outlined" value={this.state.nameToUpdate}
                                                               onChange={(e) => this.setState({nameToUpdate: e.target.value})}/>
                                                </div>
                                                <div className="form-group">
                                                    <TextField
                                                        label="Cost(per hr)"
                                                        value={this.state.costToUpdate}
                                                        onChange={(e) => this.setState({costToUpdate: e.target.value})}
                                                        name="numberformat"
                                                        id="formatted-numberformat-input"

                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter phone number' label="Phone Number" variant="outlined" value={this.state.phoneNumberToUpdate}
                                                               onChange={(e) => this.setState({phoneNumberToUpdate: e.target.value})}/>
                                            </div>
                                            </div>
                                        )}
                                        {this.state.typeToUpdate==='album' && (
                                            <div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter album name' label="Name" variant="outlined" value={this.state.nameToUpdate}
                                                               onChange={(e) => this.setState({nameToUpdate: e.target.value})}/>
                                                </div>
                                                <div className="form-group">
                                                    <TextField
                                                        label="Cost"
                                                        value={this.state.costToUpdate}
                                                        onChange={(e) => this.setState({costToUpdate: e.target.value})}
                                                        name="numberformat"
                                                        id="formatted-numberformat-input"

                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter page size' label="Page Size" variant="outlined" value={this.state.sizePageToUpdate}
                                                               onChange={(e) => this.setState({sizePageToUpdate: e.target.value})}/>
                                                </div>
                                                <div className="form-group">
                                                <TextField
                                                    id="standard-number"
                                                    label="Images Number"
                                                    type="number"
                                                    value={this.state.photosNumberToUpdate}
                                                    onChange={(e) => this.setState({photosNumberToUpdate: e.target.value})}
                                                />
                                                </div>
                                                    <div className="form-group">
                                                        <TextField
                                                            id="standard-number"
                                                            label="Pages Number"
                                                            type="number"
                                                            value={this.state.numPagesToUpdate}
                                                            onChange={(e) => this.setState({numPagesToUpdate: e.target.value})}
                                                        />
                                                    </div>
                                            </div>
                                        )}
                                        {this.state.typeToUpdate==='photo' &&
                                        (
                                            <div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter photo name' label="Name" variant="outlined" value={this.state.nameToUpdate}
                                                               onChange={(e) => this.setState({nameToUpdate: e.target.value})}/>
                                                </div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter photo category' label="Category" variant="outlined" value={this.state.categoryToUpdate}
                                                               onChange={(e) => this.setState({categoryToUpdate: e.target.value})}/>
                                                </div>
                                                <div className="form-group">
                                                    <TextField
                                                        label="Cost"
                                                        value={this.state.costToUpdate}
                                                        onChange={(e) => this.setState({costToUpdate: e.target.value})}
                                                        name="numberformat"
                                                        id="formatted-numberformat-input"

                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <TextField id="outlined-basic"
                                                               placeholder='Enter size' label="Size" variant="outlined" value={this.state.sizeToUpdate}
                                                               onChange={(e) => this.setState({sizeToUpdate: e.target.value})}/>
                                                </div>
                                            </div>
                                        )}
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-secondary"
                                                    data-dismiss="modal">cancel
                                            </button>
                                            <input className="btn btn-primary" data-dismiss="modal"
                                                   type="submit"
                                                   value="Update"
                                                   onClick={() => this.updateItem()}/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        );
    }
}

export default ManageItems;
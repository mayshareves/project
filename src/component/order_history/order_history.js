import React, {Component} from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './order_history.css'

class OrdersHistory extends  Component {

    constructor(props) {
        super(props)
        this.state.userId = this.props.location.userId.userId;
    }

    state = {
        customersList: [],
        ordersHistory: [],
        userId: ''
    };

    componentDidMount() {
        this.getCustomerData();
        // if (!this.state.intervalIsSet) {
        //     let interval = setInterval(this.getOrdersHistory, 1000);
        //     this.setState({ intervalIsSet: interval });
        // }
    }

    componentWillUnmount() {
        if (this.state.intervalIsSet) {
            clearInterval(this.state.intervalIsSet);
            this.setState({intervalIsSet: null});
        }
    }

    getCustomerData = () => {
        fetch('http://localhost:3001/api/getCustomerData')
            .then((data) => data.json())
            .then((res) => {
                res.data.forEach((customer)=>{
                    if(customer._id===this.state.userId){
                        this.setState({ordersHistory: customer.ordersHistory});
                    }
                });
             });
    }

    render(){
        return(
            <div>
                <ul >
                    {this.state.ordersHistory.length <= 0
                        ? 'NO DB ENTRIES YET'
                        : this.state.ordersHistory.map((dat) => (
                           <li style={{ padding: '10px' }}  key={dat.id}>
                               <span style={{ color: 'gray' }}> date: </span> {dat.date} <br />
                               <span style={{ color: 'gray' }}> total price: </span> ${dat.totalPrice} <br />
                                   <TableContainer component={Paper}>
                                       <Table  aria-label="customized table">
                                           <TableHead >
                                                   <TableRow className="table">
                                                       <TableCell style={{color: 'white'}}>Item Name</TableCell>
                                                       <TableCell style={{color: 'white'}}>Category</TableCell>
                                                       <TableCell style={{color: 'white'}}>Amount</TableCell>
                                                       <TableCell style={{color: 'white'}}>Price</TableCell>
                                                   </TableRow>
                                           </TableHead>
                                           <TableBody>
                                               {dat.items.map((row) => (
                                                   <TableRow key={row.data}>
                                                       <TableCell  scope="row">
                                                           {row.data}
                                                       </TableCell>
                                                       <TableCell >{row.category}</TableCell>
                                                       <TableCell >{row.amount}</TableCell>
                                                       <TableCell >{row.price}</TableCell>
                                                   </TableRow>
                                               ))}
                                           </TableBody>
                                       </Table>
                                   </TableContainer>
                           </li>
                        ))}
                </ul>
            </div>
        );
    }
}

export default OrdersHistory;

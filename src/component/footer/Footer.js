import React from 'react';
import './footer.css'
function Footer() {
    return (
        <div className="w3-agile-footer" style={{backgroundColor: "black"}}>
          <div style={{backgroundColor: "#007cc0"}}>
            <div className="container">
              <div className="row py-4 d-flex align-items-center">
                <div className="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                  <h6 className="mb-0">Get connected with us on social networks!</h6>
                </div>
                <div className="col-md-6 col-lg-7 text-center text-md-right">
                 <div className="social">
                            <ul>
                                <li><a href="https://www.facebook.com/" target='_blank' rel="noopener noreferrer"><i className="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/" target='_blank' rel="noopener noreferrer"><i className="fa fa-twitter"></i></a></li>
                                <li><a href="https://rss.com/" target='_blank' rel="noopener noreferrer"><i className="fa fa-rss"></i></a></li>
                                <li><a href="https://vk.com/" target='_blank' rel="noopener noreferrer"><i className="fa fa-vk"></i></a></li>
                            </ul>
                        </div>
                </div>
              </div>
            </div>
          </div>

          <div className="container">
                <div className="footer-grids">
                    <div className="col-md-3 footer-grid">
                        <div className="footer-grid-heading">
                            <h4>Company</h4>
                        </div>
                        <div className="footer-grid-info">
                            <ul>
                              <p>Here you can use rows and columns to organize your footer content. Lorem ipsum dolor
                                sit amet,
                                consectetur
                                adipisicing elit.</p>
                            </ul>
                        </div>
                    </div>
                    <div className="col-md-3 footer-grid">
                        <div className="footer-grid-heading">
                            <h4>Products</h4>
                        </div>
                        <div className="footer-grid-info">
                            <ul>
                                <li className="text-col">PhotoGrapher</li>
                                <li className="text-col">Painting</li>
                                <li className="text-col">Album</li>
                                <li className="text-col">Video</li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-md-3 footer-grid">
                        <div className="footer-grid-heading">
                            <h4>Useful Links</h4>
                        </div>
                        <div className="footer-grid-info">
                            <ul>
                                <li><a href="#">Your Account</a></li>
                                <li><a href="#">Become an Affiliate</a></li>
                                <li><a href="#">Shipping Rates</a></li>
                                <li><a href="#">Help</a></li>
                            </ul>
                        </div>
                    </div>
                  <div className="col-md-3 footer-grid">
                        <div className="footer-grid-heading">
                            <h4>Contact</h4>
                        </div>
                        <div className="footer-grid-info">
                          <p>
                            <i className="fa fa-home mr-3"></i> New York, NY 10012, US</p>
                          <p>
                            <i className="fa fa-envelope mr-3"></i> info@example.com</p>
                          <p>
                            <i className="fa fa-phone mr-3"></i> + 01 234 567 88</p>
                          <p>
                            <i className="fa fa-print mr-3"></i> + 01 234 567 89</p>

                        </div>
                    </div>
                    <div className="clearfix"></div>
                </div>
                <div class="footer-copyright text-center py-3" style={{color: "white"}}>© 2020 Copyright:
                <a class="dark-grey-text" href="https://mdbootstrap.com/"> MDBootstrap.com</a>
            </div>
        </div>
        </div>
    );
}

export default Footer;

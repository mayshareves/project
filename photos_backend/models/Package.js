let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let packageScheme = new Schema(
    {
        type: String,
        cost: String,
        numPhotogragher: Number,
        numHours: Number,
        typePhotos: String,
        place: String,
        image: String,
        valid: Boolean
    }
    );
let Package = mongoose.model('Package', packageScheme);
// let data = new Package({
//     type: "PLATINUM",
//     cost: "199$",
//     numPhotogragher: 1,
//     numHours: 2,
//     typePhotos: "Digital Copy",
//     place: "In Studio",
//      image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSapofDKV_rEHXxjb3HLMYjhyKAMVviJVTGkw&usqp=CAU",
//     valid: true
// });
// data.save();
// let data2 = new Package({
//     type: "SILVER",
//     cost: "299$",
//     numPhotogragher: 2,
//     numHours: 3,
//     typePhotos: "Album",
//     place: "In Studio",
//     image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS9D7ajkzfeZ115tNXsMZ_BilY37rfq-Ls8aw&usqp=CAU",
//     valid: true
// });
// data2.save();
// let data3 = new Package({
//     type: "GOLD",
//     cost: "399$",
//     numPhotogragher: 3,
//     numHours: 3,
//     typePhotos: "Digital Copy + Album",
//     place: "In Studio + outdoor",
//     image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR9vKodFkODxFYyN9D-61lO8w0-RdNVyKe91Q&usqp=CAU",
//     valid: true
// });
// data3.save();
module.exports = Package;


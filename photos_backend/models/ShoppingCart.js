let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const ShoppingCartrSchema = new Schema(
  {
    idShoppingCart: Number,
    idCustomer: String,
    items: [],
  },
    { timestamps: true }

);

let shoppingCart = mongoose.model('ShoppingCart', ShoppingCartrSchema);
module.exports = shoppingCart;

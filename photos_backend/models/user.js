const debug = require("debug")("project:model-user");
const mongo = require("mongoose");
// const passportLocalMongoose = require('passport-local-mongoose');



module.exports = db => {
    // create a schema
    let schema = new mongo.Schema({
        fullName: String,
        username: { type: String, required: true, unique: true, index: true },
        password: { type: String },
        userType: { type: String, enum : ['client','worker','manager'], default: 'client'},
        address: { type: String},
        created_at: Date,
        updated_at: Date
    }, { autoIndex: false });


    schema.statics.CREATE = async function(user) {
        return this.create(user);
    };



    schema.statics.REQUEST = async function() {
        // no arguments - bring all at once
        const args = Array.from(arguments);
        if (args.length === 0) {
            debug("request: no arguments - bring all at once");
            return this.find({}).exec();
        }

        // perhaps last argument is a callback for every single document
        let callback = arguments[arguments.length - 1];
        if (callback instanceof Function) {
            let asynch = callback.constructor.name === 'AsyncFunction';
            debug(`request: with ${asynch?'async':'sync'} callback`);
            args.pop();
            let cursor, user;
            try {
                cursor = await this.find(...args).cursor();
            } catch (err) { throw err; }
            try {
                while (null !== (user = await cursor.next())) {
                    if (asynch) {
                        try {
                            await callback(user);
                        } catch (err) { throw err; }
                    }
                    else {
                        callback(user);
                    }
                }
            } catch (err) { throw err; }
            return;
        }

        // request by id as a hexadecimal string
        if (args.length === 1 && typeof args[0] === "string") {
            debug("request: by ID");
            return this.findById(args[0]).exec();
        }

        // There is no callback - bring requested at once
        debug(`request: without callback: ${JSON.stringify(args)}`);
        return this.find(...args).exec();
    }

    // schema.plugin(passportLocalMongoose);

    // the schema is useless so far
    // we need to create a model using it
    // db.model('Usner', schema, 'User'); // (model, schema, collection)
    db.model('User', schema); // if model name === collection name
    debug("User model created");
}

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let workerScheme = new Schema(
    {
        fullName: String,
        userName: String,
        password: String,
        mail: String,
        phone: String,
        address: String,
        image: String,
        isConnected: Boolean,
        valid: Boolean
    },
    { timestamps: true }
);
let worker = mongoose.model('worker', workerScheme)
module.exports = worker;
const debug = require("debug")("project:model");
const mongo = require("mongoose");

let db = mongo.createConnection();
(async () => {
    try {
        await db.openUri('mongodb://localhost/dataBasePhotoWall', {useNewUrlParser: true, useUnifiedTopology: true});
    } catch (err) {
        debug("Error connecting to DB: " + err);
    }
})();
debug('Pending DB connection');

require("./user")(db);

module.exports = model => db.model(model);
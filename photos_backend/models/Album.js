let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let AlbumScheme = new Schema(
    {
        name: String,
        image: String,
        sizePage: String,
        image: String,
        photosNumber: Number,
        numPage: Number,
        cost: String,
        valid: Boolean
    }
);
let album = mongoose.model('album', AlbumScheme);


// let data = new album({
//     name: "baybe",
//     sizePage: "20X30cm",
//     image: "images/bybeAlbum.PNG",
//     photosNumber: "15",
//     numPage: 8,
//     cost: "40$",
//     valid: true
// });
// data.save();
// let data2 = new album({
//         name: "family",
//         sizePage: "25X35cm",
//         image: "images/familyAlbum.PNG",
//         photosNumber: "20",
//         numPage: 10,
//         cost: "50$",
//         valid: true
// });
// data2.save();
// let data3 = new album({
//         name: "summer",
//         sizePage: "15X20cm",
//         image: "images/summerAlbum.PNG",
//         photosNumber: "10",
//         numPage: 6,
//         cost: "25",
//         valid: true
// });
// data3.save();
module.exports = album;
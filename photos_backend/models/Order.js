let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const OrderSchema = new Schema(
  {
    idOrder: Number,
    sum: Number,
    date: Date,
    customerId: Number
  }
);

let order = mongoose.model('Order', OrderSchema);
module.exports = order;

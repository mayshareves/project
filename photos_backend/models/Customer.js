let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let cstScheme = new Schema(
    {
        fullName: String,
        userName: String,
        password: String,
        mail: String,
        phone: String,
        address: String,
        ordersHistory: Array,
        rooms: Array,
        image: String,
        typeLogin: String,
        googleId: String,
        isConnected: Boolean,
        valid: Boolean
    },
    { timestamps: true }
    );

let Customer = mongoose.model('Customer', cstScheme);

module.exports = Customer;





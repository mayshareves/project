let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let photographerScheme = new Schema(
    {
        name: String,
        image: String,
        phoneNumber: String,
        cost: String,
        valid: Boolean
    }
);
let photographer = mongoose.model('Photographer', photographerScheme);
// let data = new photographer({
//         name: "Williamson",
//         image: "images/team1.jpg",
//         phoneNumber: "0527387145",
//         cost: "$50",
//         valid: true
// });
// data.save();
// let data2 = new photographer({
//         name: "Ian Brown",
//         image: "images/team3.jpg",
//         phoneNumber: "0506843297",
//         cost: "$60",
//         valid: true
// });
// data2.save();
// let data3 = new photographer({
//         name: "Pavi",
//         image: "images/team4.jpg",
//         phoneNumber: "0564895132",
//         cost: "$75",
//         valid: true
// });
// data3.save();
// let data4 = new photographer({
//         name: "Leslie Larsen",
//         image: "images/team5.jpg",
//         phoneNumber: "0506485231",
//         cost: "$80",
//         valid: true
// });
// data4.save();
// let data5 = new photographer({
//         name: "Natalie Bre",
//         image: "images/team6.jpg",
//         phoneNumber: "0548795412",
//         cost: "$85",
//         valid: true
// });
// data5.save();
module.exports = photographer;





const mongoose = require('mongoose');
const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const Data = require('./Data');
const CustomerData = require('./Customer');
const ShoppingCartData = require('./ShoppingCart');
const mngData = require('./Managment');
const albumData = require('./Album');
const packageData = require('./Package');
const photographerData = require('./Photographer');
const photoData = require('./Photo');
const psw = require('./pswSecret');
const workers = require('./worker');
// var debug = require('debug')('project:server');
let crypto = require('crypto');

const API_PORT = 3001;
const app = express();
app.use(cors());
app.use(bodyParser.json({limit: '50mb', extended: true}))
const router = express.Router();

// this is our MongoDB database
const dbRoute =
    'mongodb+srv://mayshar:Av5*B@cluster2-3usr9.mongodb.net/test?retryWrites=true&w=majority';

// connects our back end code with the database
mongoose.connect(dbRoute, { useNewUrlParser: true });
let db = mongoose.connection;
db.once('open', () => console.log('connected to the database'));

// checks if connection with the database is successful
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// (optional) only made for logging and
// bodyParser, parses the request body to be a readable json format
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));

// this is our get method
// this method fetches all available data in our database
router.get('/getData', (req, res) => {
  Data.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});
router.get('/getpsw', (req, res) => {
  psw.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});
router.get('/getWorkers', (req, res) => {
  workers.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});
router.get('/getAlbumData', (req, res) => {
  albumData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});
router.get('/getPackages', (req, res) => {
  packageData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});
router.get('/getPhotoData', (req, res) => {
  photoData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});
router.get('/getCustomerData', (req, res) => {
  CustomerData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
})
router.get('/getPhotograghersData', (req, res) => {
  photographerData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});
router.get('/getOrdersHIstory', (req, res) => {
  CustomerData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    data.forEach((customer)=>{
      if(customer.isConnected===true){
        console.log(customer.ordersHistory);
        return res.json({ success: true, data: customer.ordersHistory });
      }
    });

  });
});
router.get('/getShoppingCartsData', (req, res) => {
  ShoppingCartData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});
router.get('/getManagementData', (req, res) => {
  mngData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});

// this is our update method
// this method overwrites existing data in our database
router.post('/updateData', (req, res) => {
  const { id, update } = req.body;
  Data.findByIdAndUpdate(id, update, (err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

router.post('/authenticateUser', (req, res) => {
    const { userName, password} = req.body;
    let login = false;
    CustomerData.findOne({userName: userName}).then((result)=>{
        if(result!==null && password===result.password){
            return res.json({exists:true, userId: result._id, permission: 'customer' });
        }else {
            workers.findOne({userName: userName}).then((result) => {
                if (result !== null && password === result.password) {
                    return res.json({exists: true, userId: result._id, permission: 'worker'});
                } else{
                    mngData.findOne({userName: userName}).then((result)=>{
                        if(result!==null && password===result.password){
                            return res.json({exists:true,  userId: result._id, permission: 'management' });
                        }else{
                            return res.json({exists:false,  userId: null, permission: null });
                        }
                    });
                }
            });
        }
    });
});

router.post('/updateCustomer', (req, res) => {
    const { id , update } = req.body;
    CustomerData.findByIdAndUpdate(id,update, (err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });
});
router.post('/updateWorker', (req, res) => {
    const { id , update } = req.body;
    workers.findByIdAndUpdate(id,update, (err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });
});
router.post('/updateManagement', (req, res) => {
    const { id , update } = req.body;
    mngData.findByIdAndUpdate(id,update, (err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });
});
router.post('/deletePhotoItem', (req, res) => {
    const { id , update } = req.body;
    photoData.findByIdAndUpdate(id, update, (err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });
});
router.post('/deleteAlbumItem', (req, res) => {
    const { id, update } = req.body;
    albumData.findByIdAndUpdate(id, update, (err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });
});
router.post('/deletePhotographerItem', (req, res) => {
    const { id , update } = req.body;
    photographerData.findByIdAndUpdate(id,update, (err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });
});
router.post('/deletePackageItem', (req, res) => {
    const { id, update } = req.body;
    packageData.findByIdAndUpdate(id, update, (err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });
    // packageData.findByIdAndRemove(id, (err) => {
    //   if (err) return res.send(err);
    //   return res.json({ success: true });
    // });
});
router.post('/updateCartItems', (req, res) => {
    const { id, update } = req.body;

    ShoppingCartData.findByIdAndUpdate(id, update, (err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });

});

// this is our delete method
// this method removes existing data in our database
router.delete('/deleteData', (req, res) => {
  const { id } = req.body;
  Data.findByIdAndRemove(id, (err) => {
    if (err) return res.send(err);
    return res.json({ success: true });
  });
});

router.delete('/deleteCustomer', (req, res) => {
    const { id } = req.body;
    CustomerData.findByIdAndRemove(id, (err) => {
        if (err) return res.send(err);
        return res.json({ success: true });
    });
});

router.delete('/deleteWorker', (req, res) => {
    const { id } = req.body;
    workers.findByIdAndRemove(id, (err) => {
        if (err) return res.send(err);
        return res.json({ success: true });
    });
});
router.delete('/deleteManager', (req, res) => {
    const { id } = req.body;
    mngData.findByIdAndRemove(id, (err) => {
        if (err) return res.send(err);
        return res.json({ success: true });
    });
});
// this is our create methid
// this method adds new data in our database
router.post('/putData', (req, res) => {
  let data = new Data();

  const { id, message } = req.body;

  if ((!id && id !== 0) || !message) {
    return res.json({
      success: false,
      error: 'INVALID INPUTS',
    });
  }
  data.message = message;
  data.id = id;
  data.save((err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});
router.post('/putNewClient', (req, res) => {
  let customer = new CustomerData();

  const { name, username, password, mail, phone, address, image } = req.body;
  if (!name)  {
    return res.json({
      success: false,
      error: 'INVALID INPUTS',
    });
  }
  customer.fullName = name;
  customer.userName = username;
  customer.password = password;
  customer.mail = mail;
  customer.phone = phone;
  customer.address = address;
  customer.image = image;
  customer.valid=true;
  customer.isConnected=false;
  customer.rooms = [];
  // debug(data);
  customer.save((err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true});
  });
});
router.post('/putNewClientGoogle', (req, res) => {
    let customer = new CustomerData();
    const { name, userName, mail, googleId, image, typeLogin } = req.body;
    customer.fullName = name;
    customer.userName = userName;
    customer.mail = mail;
    customer.googleId = googleId;
    customer.image = image;
    customer.valid=true;
    customer.isConnected=false;
    customer.typeLogin=typeLogin;
    customer.rooms = [];
    customer.save((err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, id: customer._id});
    });
});


router.post('/putNewWorker', (req, res) => {
  let worker = new workers();

  const { name, username, password, mail, phone, address, image  } = req.body;
  if (!name)  {
    return res.json({
      success: false,
      error: 'INVALID INPUTS',
    });
  }
    worker.fullName = name;
    worker.userName = username;
    worker.password = password;
    worker.mail = mail;
    worker.phone = phone;
    worker.address = address;
    worker.image = image;
    worker.valid = true;
    worker.isConnected = false;
  // debug(data);
  worker.save((err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true});
  });
});
router.post('/putNewManagement', (req, res) => {
    let management = new mngData();

    const {name, username, password, mail, phone, address, image} = req.body;
    if (!name) {
        return res.json({
            success: false,
            error: 'INVALID INPUTS',
        });
    }
    management.fullName = name;
    management.userName = username;
    management.password = password;
    management.mail = mail;
    management.phone = phone;
    management.address = address;
    management.image = image;
    management.valid = true;
    management.isConnected = false;
    // debug(data);
    management.save((err) => {
        if (err) return res.json({success: false, error: err});
        return res.json({success: true});
    });
});
router.post('/putClientLogin', (req, res) => {
  const { id, update } = req.body;

  CustomerData.findByIdAndUpdate(id, update, (err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });

});
router.post('/putworkerLogin', (req, res) => {
  const { id, update } = req.body;

  workers.findByIdAndUpdate(id, update, (err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });

});
router.post('/putmanagementLogin', (req, res) => {
  const { id, update } = req.body;

  mngData.findByIdAndUpdate(id, update, (err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });

});
router.post('/putNewItemToBassket', (req, res) => {
  const { id, update } = req.body;

  ShoppingCartData.findByIdAndUpdate(id, update, (err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });

});
router.post('/putBasket', (req, res) => {
  let cart=new ShoppingCartData();
  const { idCustomer, items } = req.body;
  cart.idCustomer = idCustomer;
  cart.items = items;
  cart.save((err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, id: cart._id });
  });

});

// append /api for our http requests
app.use('/api', router);

// launch our backend into a port
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));
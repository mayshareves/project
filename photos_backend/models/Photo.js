let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const PhotoSchema = new Schema(
  {
    cost: String,
    name: String,
    image: String,
    category: String,
    size: String,
    valid: Boolean
  }
);

let Photo = mongoose.model('Photo', PhotoSchema);
// let data5 = new Photo({
//     cost: "$10",
//     name: "bambi",
//     image: "images/bambiPhoto.PNG",
//     category: "animals",
//     size: "30X25cm",
//     valid: true
// });
// data5.save();
// let data = new Photo({
//     cost: "$15",
//     name: "giraph",
//     image: "images/giraphPhoto.PNG",
//     category: "animals",
//     size: "50X30cm",
//     valid: true
// });
// data.save();
// let data2 = new Photo({
//     cost: "$20",
//     name: "ship",
//     image: "images/sheepPhoto.PNG",
//     category: "view",
//     size: "40X55cm",
//     valid: true
// });
// data2.save();
// let data3 = new Photo({
//     cost: "$30",
//     name: "tree",
//     image: "images/treePhoto.PNG",
//     category: "view",
//     size: "80X40cm",
//     valid: true
// });
// data3.save();
// let data4 = new Photo({
//     cost: "$10",
//     name: "vered",
//     image: "images/veredPhoto.JPG",
//     category: "flowers",
//     size: "20X20cm",
//     valid: true
// });
// data4.save();
module.exports = Photo;

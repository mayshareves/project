const debug = require("debug")("mongo:model-manager");
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let mngScheme = new Schema(
    {
        fullName: String,
        userName: String,
        password: String,
        mail: String,
        phone: String,
        address: String,
        image: String,
        isConnected: Boolean,
        valid: Boolean
    },
    { timestamps: true }
);

let Management = mongoose.model('Management', mngScheme);

module.exports = Management;
